package com.asiapacificgroup;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.modals.LoginResponseTemplate;
import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by manishautomatic on 07/02/17.
 */
public class ActivityMakeApplication  extends AppCompatActivity implements View.OnClickListener {


    private TextView mTxtVwDateOfBirth;
    Calendar calendar;
    private EditText mEdTxtName,mEdtxtEmail,mEdTxtMobileNumber;
    private Spinner mSpnrEducationalQualification,mSpnrWorkExperience,mSpnrLookingFor,mSpnrIeltsBands;
    private CheckBox mCkhBxAustrailia,mChkBoxCanada,mChkBoxNewZealand,mChkBxOthers;
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE=8;

    boolean isOkayClicked = false;
    int byear, bmonth, bday;
    TextView tvSetDate;
    private RelativeLayout mRelLytSubmitApplication;
    private RadioButton mRdBtnMale, mRdBtnFemale, mRdBtnIeltsYes, mRdBtnIeltsNo;
    private LinearLayout mLinLytIELTSBands;
    private ProgressDialog pDialog;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
       // getSupportActionBar().setDisplayUseLogoEnabled(true);
        //getSupportActionBar().setIcon(R.drawable.site_logo);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().hide();
        //getSupportActionBar().setIcon(R.drawable.site_logo);
        //getSupportActionBar().setCustomView(R.layout.custom_action_bar);

        overridePendingTransition(R.anim.slide_in_from_below, R.anim.slide_in_right);
        setContentView(R.layout.activity_make_application);
        initUI();
        setupClickListners();
        initCalendar();
        prepareFooterViews();
        pDialog=new ProgressDialog(ActivityMakeApplication.this);
    }


    private void initUI(){
        mTxtVwDateOfBirth=(TextView)findViewById(R.id.edt_dob);
        mRdBtnMale=(RadioButton)findViewById(R.id.rb_male);
        mRdBtnFemale=(RadioButton)findViewById(R.id.rb_female);
        mRdBtnIeltsYes=(RadioButton)findViewById(R.id.rb_ielts_yes);
        mRdBtnIeltsNo=(RadioButton)findViewById(R.id.rb_ielts_no);
        mLinLytIELTSBands=(LinearLayout)findViewById(R.id.view_ielts_bands);
        mRelLytSubmitApplication=(RelativeLayout)findViewById(R.id.relLytSubmitApplication);
        // init the application form controls...
        mEdTxtName = (EditText)findViewById(R.id.edt_firstName);
        mEdtxtEmail= (EditText)findViewById(R.id.edt_email);
        mEdTxtMobileNumber= (EditText)findViewById(R.id.edt_mobile_number);
        mSpnrEducationalQualification=(Spinner)findViewById(R.id.sp_qualification);
        mSpnrWorkExperience=(Spinner)findViewById(R.id.sp_work_experience);
        mSpnrIeltsBands=(Spinner)findViewById(R.id.sp_ielts_bands);
        mSpnrLookingFor=(Spinner)findViewById(R.id.sp_visa_type);
        mCkhBxAustrailia=(CheckBox)findViewById(R.id.chkbxAustrailia);
        mChkBoxCanada=(CheckBox)findViewById(R.id.chkbxCanada);
        mChkBoxNewZealand=(CheckBox)findViewById(R.id.chkbxnewzealand);
        mChkBxOthers=(CheckBox)findViewById(R.id.chkbxOthers);

    }

    private void setupClickListners(){
        mTxtVwDateOfBirth.setOnClickListener(this);
        mRelLytSubmitApplication.setOnClickListener(this);
        mRdBtnMale.setOnClickListener(this);
        mRdBtnFemale.setOnClickListener(this);
        mRdBtnIeltsYes.setOnClickListener(this);
        mRdBtnIeltsNo.setOnClickListener(this);
    }


    private void initCalendar(){
        calendar = Calendar.getInstance();
        byear = calendar.get(Calendar.YEAR);
        bmonth = calendar.get(Calendar.MONTH);
        bday = calendar.get(Calendar.DAY_OF_MONTH);
    }

    private void prepareFooterViews(){
        LinearLayout lytDialier = (LinearLayout)findViewById(R.id.initCall);
        LinearLayout lytEnqury = (LinearLayout)findViewById(R.id.initEnquiry);

        TextView txtvwDialer = (TextView)findViewById(R.id.txtvwDialer);
        txtvwDialer.setText("Call:" + ActivityMakeApplication.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", ""));

        lytDialier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if permission to make calls has been granted..
                if (ContextCompat.checkSelfPermission(ActivityMakeApplication.this,
                        android.Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityMakeApplication.this,
                            android.Manifest.permission.CALL_PHONE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(ActivityMakeApplication.this,
                                new String[]{android.Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_CALL_PHONE);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }else{
                    // permission is available, initiate call action.
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + ActivityMakeApplication.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", "")));
                    startActivity(intent);
                }
            }
        });
        lytEnqury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApgAppInstance.PAGE_TO_LOAD=ApgAppInstance.ENQUIRY_LINK;
                Intent intent = new Intent(ActivityMakeApplication.this,ActivityLocalWebContent.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view==mTxtVwDateOfBirth){
            showDateAlertDialog(mTxtVwDateOfBirth);
        }if(view==mRdBtnIeltsYes){
                mLinLytIELTSBands.setVisibility(View.VISIBLE);
        }if(view==mRdBtnIeltsNo){
            mLinLytIELTSBands.setVisibility(View.GONE);
        }if(view==mRelLytSubmitApplication){
            //startActivity(new Intent(ActivityMakeApplication.this,ActivityApplicationSubmitted.class));
            //finish();
            postApplication();
        }
    }

    @SuppressLint("NewApi")
    private void showDateAlertDialog(TextView tvSetDate) {
            this.tvSetDate = tvSetDate;
            String date = "";
            final DateSetListener dataSetListener = new DateSetListener();
            final DatePickerDialog datePicker = new DatePickerDialog(ActivityMakeApplication.this,
                                                    dataSetListener, byear, bmonth, bday);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                datePicker.getDatePicker().setMaxDate(calendar.getTimeInMillis());
            } else {
                final int minYear = calendar.get(Calendar.YEAR);
                final int minMonth = calendar.get(Calendar.MONTH);
                final int minDay = calendar.get(Calendar.DAY_OF_MONTH);
                Field mDatePickerField = null;
                try {
                    mDatePickerField = datePicker.getClass().getDeclaredField("mDatePicker");
                } catch (NoSuchFieldException e) {

                    e.printStackTrace();
                }
                mDatePickerField.setAccessible(true);
                DatePicker mDatePickerInstance = null;
                try {
                    mDatePickerInstance = (DatePicker) mDatePickerField.get(datePicker);
                } catch (IllegalArgumentException e) {

                    e.printStackTrace();
                } catch (IllegalAccessException e) {

                    e.printStackTrace();
                }

                mDatePickerInstance.init(minYear, minMonth, minDay, new DatePicker.OnDateChangedListener() {

                    public void onDateChanged(DatePicker view, int year, int month, int day) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, month, day);
                        if (newDate.after(calendar)) {
                            view.init(minYear, minMonth, minDay, this);
                        }
                    }
                });

            }
            datePicker.setButton(DialogInterface.BUTTON_NEGATIVE, ActivityMakeApplication.this
                        .getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                @SuppressLint("NewApi")
                public void onClick(DialogInterface dialog, int which) {
                    isOkayClicked = false;

                }
            });
            datePicker.setButton(DialogInterface.BUTTON_POSITIVE, ActivityMakeApplication.this.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @SuppressLint("NewApi")
                public void onClick(DialogInterface dialog, int which) {
                    isOkayClicked = true;
                    DatePicker datePickerDialog = datePicker.getDatePicker();
                    dataSetListener.onDateSet(datePickerDialog, datePickerDialog.getYear(), datePickerDialog.getMonth(), datePickerDialog.getDayOfMonth());
                }
            });
            datePicker.show();

    }

    private class DateSetListener implements DatePickerDialog.OnDateSetListener {

        public void onDateSet(DatePicker view, int year, int month, int day) {

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));

            byear = year;
            bmonth = month + 1;
            bday = day;
            Calendar c = Calendar.getInstance();
            c.set(year, month, day);
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String selectedDate = df.format(c.getTime());
            //Checking if OK clicked
            if (isOkayClicked) {
                tvSetDate.setText(selectedDate);
                isOkayClicked = false;
            }
            bmonth = month;
        }
    }


   // post application to server...

    private void postApplication(){
        final String Name = mEdTxtName.getText().toString();
        if(Name==null || Name.trim().equalsIgnoreCase("")){
            Toast.makeText(ActivityMakeApplication.this,"Please provide applicant name",Toast.LENGTH_LONG).show();
            return;
        }
      final  String email = mEdtxtEmail.getText().toString();
        if(!isValidEmail(email)){
            Toast.makeText(ActivityMakeApplication.this,"Please provide valid email",Toast.LENGTH_LONG).show();
            return;
        }

        final String mobileNumber = mEdTxtMobileNumber.getText().toString();
        if(mobileNumber==null || mobileNumber.trim().equalsIgnoreCase("")|| mobileNumber.length()<10){
            Toast.makeText(ActivityMakeApplication.this,"Please provide valid mobile",Toast.LENGTH_LONG).show();
            return;
        }
        String dob = mTxtVwDateOfBirth.getText().toString();
        if(dob.trim().equalsIgnoreCase("")){
            Toast.makeText(ActivityMakeApplication.this,"Please provide date of birth",Toast.LENGTH_LONG).show();
            return;
        }
        final String qualification = mSpnrEducationalQualification.getSelectedItem().toString();
        if(qualification.equalsIgnoreCase("Select Qualification")){
            Toast.makeText(ActivityMakeApplication.this,"Please provide qualification",Toast.LENGTH_LONG).show();
            return;
        }
        final String workEx = mSpnrWorkExperience.getSelectedItem().toString();
        if(workEx.equalsIgnoreCase("Select Experience")){
            Toast.makeText(ActivityMakeApplication.this,"Please provide work experience",Toast.LENGTH_LONG).show();
            return;
        }
        final String lookingFor = mSpnrLookingFor.getSelectedItem().toString();
        if(lookingFor.equalsIgnoreCase("Select VISA type")){
            Toast.makeText(ActivityMakeApplication.this,"Please provide visa requirement",Toast.LENGTH_LONG).show();
            return;
        }
        String ieltsTaken = "N";
        String ieltsBands="";
        if(mRdBtnIeltsYes.isChecked()){
            ieltsTaken="Y";
             ieltsBands = mSpnrIeltsBands.getSelectedItem().toString();
        }
        final String ieltsTaken_=ieltsTaken;
        final String ieltsBands_=ieltsBands;
        String _au="N";
        String _ca="N";
        String _nz="N";
        String _ot="N";
        if(mCkhBxAustrailia.isChecked()){
            _au="Y";
        }if(mChkBoxCanada.isChecked()){
            _ca="Y";
        }if(mChkBoxNewZealand.isChecked()){
            _nz="Y";
        }if(mChkBxOthers.isChecked()){
            _ot="Y";
        }

        final String _au_=_au;
        final String _ca_=_ca;
        final String _nz_=_nz;
        final String _ot_=_ot;
        String maritalStatus="M";
        if(mRdBtnFemale.isChecked()){
            maritalStatus="M";
        }else{
            maritalStatus="S";
        }
        final String  maritalStatus_=maritalStatus;

        StringRequest sr = new StringRequest(Request.Method.POST, ApgAppInstance.EP_CREATE_APPLICATION , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("", response.toString());
                Gson gson = new Gson();
                LoginResponseTemplate loginResponse = (LoginResponseTemplate)gson.fromJson(response
                        ,LoginResponseTemplate.class);

                if(loginResponse.getResponse_code().equalsIgnoreCase("1")){
                    //Toast.makeText(ActivityMakeApplication.this,loginResponse.getResponse_message(),Toast.LENGTH_LONG).show();
                    ApgAppInstance.APPICATION_NUMBER=loginResponse.getApplication_number();
                    startActivity(new Intent(ActivityMakeApplication.this,ActivityApplicationSubmitted.class));
                    finish();

                }else{
                    Toast.makeText(ActivityMakeApplication.this,loginResponse.getResponse_message(),Toast.LENGTH_LONG).show();
                }
                pDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ActivityMakeApplication.this,"could not file application, please try again",Toast.LENGTH_LONG).show();
                VolleyLog.d("", "Error: " + error.getMessage());
                Log.d("", ""+error.getMessage()+","+error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                String dob_=Integer.toString(byear)+"-"+Integer.toString(bmonth)+"-"+Integer.toString(bday);
                String user_id = ActivityMakeApplication.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                                .getString("user_id","");
                Map<String, String> params = new HashMap<String, String>();
                params.put("applicant_name", Name);
                params.put("applicant_email", email);
                params.put("applicant_mobile", mobileNumber);
                params.put("marital_status", maritalStatus_);
                params.put("applicant_qualification", qualification);
                params.put("work_experience", workEx);
                params.put("ielts_taken", ieltsTaken_);
                params.put("ielts_bands", ieltsBands_);
                params.put("looking_for", lookingFor);
                params.put("intrested_au", _au_);
                params.put("intrested_ca", _ca_);
                params.put("intrested_nz", _nz_);
                params.put("intrested_ot", _ot_);
                params.put("applicant_dob", dob_);
                params.put("user_id",user_id );
                return params;
            }

            /*
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<String, String>();
                headers.put("Content-Type","application/x-www-form-urlencoded");
                headers.put("abc", "value");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }*/
        };
        sr.setShouldCache(false);
        ApgAppInstance.getAppInstance().addToRequestQueue(sr,"file");
    }




    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
