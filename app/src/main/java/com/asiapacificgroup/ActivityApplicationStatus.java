package com.asiapacificgroup;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.asiapacificgroup.controller.ApgAppInstance;

import org.w3c.dom.Text;

/**
 * Created by manishautomatic on 07/02/17.
 */
public class ActivityApplicationStatus extends AppCompatActivity {


    private TextView mTxtVwApplicationNumber,
                     mTxtVwApplicationStatus,
                     mTxtVwApplicantName,
                     mTxtVwSubmissionDate,
                     mtxtVwApplicantEmail,
                     mTxtVwApplicationType;
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE=8;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        overridePendingTransition(R.anim.slide_in_from_below, R.anim.slide_in_right);
        setContentView(R.layout.activity_application_status);
        prepareFooterViews();
        initUI();


    }


    private void initUI(){
        mTxtVwApplicationNumber=(TextView)findViewById(R.id.txtvwStatusApplicationNumber);
        mTxtVwApplicationStatus=(TextView)findViewById(R.id.txtvwStatus);
        mTxtVwApplicationType=(TextView)findViewById(R.id.txtvwApplicationType);
        mTxtVwSubmissionDate=(TextView)findViewById(R.id.txtvwFilingDate);
        mTxtVwApplicantName=(TextView)findViewById(R.id.txtvApplicantName);
        mtxtVwApplicantEmail=(TextView)findViewById(R.id.txtvwApplicantEmail);

        mTxtVwApplicationNumber.setText(ApgAppInstance.APPICATION_NUMBER);
        mTxtVwApplicationStatus.setText(ApgAppInstance.APPICATION_STATUS);
        mTxtVwApplicationType.setText(ApgAppInstance.APPICATION_TYPE);
        mTxtVwSubmissionDate.setText(ApgAppInstance.APPICATION_FILING_DATE);
        mTxtVwApplicantName.setText(ApgAppInstance.APPICANT_NAME);
        mtxtVwApplicantEmail.setText(ApgAppInstance.APPICANT_EMAIL);
    }


    private void prepareFooterViews(){
        LinearLayout lytDialier = (LinearLayout)findViewById(R.id.initCall);
        LinearLayout lytEnqury = (LinearLayout)findViewById(R.id.initEnquiry);
        TextView txtvwDialer = (TextView)findViewById(R.id.txtvwDialer);
        txtvwDialer.setText("Call:" + ActivityApplicationStatus.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", ""));

        lytDialier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if permission to make calls has been granted..
                if (ContextCompat.checkSelfPermission(ActivityApplicationStatus.this,
                        android.Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityApplicationStatus.this,
                            android.Manifest.permission.CALL_PHONE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(ActivityApplicationStatus.this,
                                new String[]{android.Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_CALL_PHONE);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }else{
                    // permission is available, initiate call action.
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + ApgAppInstance.PHONE_NUMBER));
                    startActivity(intent);
                }
            }
        });
        lytEnqury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApgAppInstance.PAGE_TO_LOAD = ApgAppInstance.ENQUIRY_LINK;
                Intent intent = new Intent(ActivityApplicationStatus.this, ActivityLocalWebContent.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
