package com.asiapacificgroup;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.controller.JsInterface;

/**
 * Created by manishautomatic on 07/02/17.
 */

public class ActivityLocalWebContent extends AppCompatActivity {

    private WebView mWebView;
    private ProgressBar mProgressBar;
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE=8;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        getSupportActionBar().hide();
        overridePendingTransition(R.anim.slide_in_from_below, R.anim.slide_in_right);

        setContentView(R.layout.activity_local_web_content);
        initUI();
        prepareFooterViews();
    }

    private void initUI(){
        mWebView=(WebView)findViewById(R.id.wvLocalContent);
        JsInterface jsInterface = new JsInterface(this);
        mProgressBar=(ProgressBar)findViewById(R.id.ProgressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.addJavascriptInterface(jsInterface, "JsInterface");
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.loadUrl(ApgAppInstance.PAGE_TO_LOAD);
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if(progress>95){
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });
    }



    // our custom webview client to control page navigations..

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return false;
        }
    }


    private void prepareFooterViews(){
        LinearLayout lytDialier = (LinearLayout)findViewById(R.id.initCall);
        LinearLayout lytEnqury = (LinearLayout)findViewById(R.id.initEnquiry);
        TextView txtvwDialer = (TextView)findViewById(R.id.txtvwDialer);
        txtvwDialer.setText("Call:" + ActivityLocalWebContent.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", ""));

        lytDialier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if permission to make calls has been granted..
                if (ContextCompat.checkSelfPermission(ActivityLocalWebContent.this,
                        android.Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityLocalWebContent.this,
                            android.Manifest.permission.CALL_PHONE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(ActivityLocalWebContent.this,
                                new String[]{android.Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_CALL_PHONE);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }else{
                    // permission is available, initiate call action.
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + ActivityLocalWebContent.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", "")));
                    startActivity(intent);
                }
            }
        });
        lytEnqury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApgAppInstance.PAGE_TO_LOAD=ApgAppInstance.ENQUIRY_LINK;
                Intent intent = new Intent(ActivityLocalWebContent.this,ActivityLocalWebContent.class);
                startActivity(intent);

            }
        });
    }


    @Override
    public void onBackPressed(){
        finish();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
