package com.asiapacificgroup;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;

import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.asiapacificgroup.controller.Constants;
import com.asiapacificgroup.controller.FetchAddressIntentService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;


import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by manishautomatic on 07/02/17.
 */
public class ActivitySplash extends AppCompatActivity implements View.OnClickListener, ConnectionCallbacks,
        OnConnectionFailedListener,
        LocationListener {

    private LocationManager locationManager;
    private String provider;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;
    protected Boolean mRequestingLocationUpdates;
    protected String mLastUpdateTime;
    protected static final String TAG = "SplashActivity";
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private AddressResultReceiver mResultReceiver;
    private ProgressBar mProgressBar;
    private RelativeLayout mRelLytIn, mRelLytAU, mRelLytOT;


    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_splash);
        mProgressBar=(ProgressBar)findViewById(R.id.ProgressBar);
       // mProgressBar.setVisibility(View.VISIBLE);
        mResultReceiver = new AddressResultReceiver(new Handler());
        String COUNTRY=ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE).getString("country","");
        if(COUNTRY.equalsIgnoreCase("")){

        }else{
            startActivity(new Intent(ActivitySplash.this, ActivityHome.class));
            finish();
        }

       //

        initUI();
    }


    private void initUI(){
        mRelLytAU=(RelativeLayout)findViewById(R.id.rellytAUS);
        mRelLytIn=(RelativeLayout)findViewById(R.id.rellytIN);
        mRelLytOT=(RelativeLayout)findViewById(R.id.rellytOthers);

        mRelLytAU.setOnClickListener(this);
        mRelLytIn.setOnClickListener(this);
        mRelLytOT.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
       // mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates if the user has requested them.
       // if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
         //   startLocationUpdates();
        //}
        updateUI();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
       // if (mGoogleApiClient.isConnected()) {
        //    stopLocationUpdates();
        //}
    }

    @Override
    protected void onStop() {
        super.onStop();
     //   mGoogleApiClient.disconnect();
    }



    private void initLocation(){
        showCountrySelector();

    }


    private void initLocationClients(){
        mRequestingLocationUpdates = true;
        mLastUpdateTime = "";
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
    }

    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }



    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        LocationServices.SettingsApi.checkLocationSettings(
                mGoogleApiClient,
                mLocationSettingsRequest
        ).setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");



                        try {
                            LocationServices.FusedLocationApi
                                    .requestLocationUpdates(
                                            mGoogleApiClient, mLocationRequest, ActivitySplash.this);
                        } catch (Exception e) {

                        }


                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                "location settings ");
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the
                            // result in onActivityResult().
                            status.startResolutionForResult(ActivitySplash.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        String errorMessage = "Location settings are inadequate, and cannot be " +
                                "fixed here. Fix in Settings.";
                        Log.e(TAG, errorMessage);
                        Toast.makeText(ActivitySplash.this, errorMessage, Toast.LENGTH_LONG).show();
                        mRequestingLocationUpdates = false;
                }
                updateUI();
            }
        });

    }



    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                ActivitySplash.this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
                //setButtonsEnabledState();
            }
        });
    }




    private void updateUI(){

    }

    private void showCountrySelector(){
        CharSequence colors[] = new CharSequence[]{"India", "Austrailia", "Quit the App"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySplash.this);
        builder.setTitle("Select your country");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.e("value is", "" + which);
                switch (which) {
                    case 0:
                        ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                                    .edit().putString("country","in").commit();
                        new SplashAsync().execute();
                        break;
                    case 1:
                        ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                                .edit().putString("country","au").commit();
                        new SplashAsync().execute();
                        break;
                    case 2:
                        ActivitySplash.this.finish();
                        break;
                }
            }
        });
        builder.show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mCurrentLocation == null) {
           // mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            //updateLocationUI();
        }
        if (mRequestingLocationUpdates) {
            Log.i(TAG, "in onConnected(), starting location updates");
            startLocationUpdates();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
            if(location!=null){
                mCurrentLocation=location;
                stopLocationUpdates();
                startIntentService(location);
            }
    }


    protected void startIntentService(Location location) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(this, FetchAddressIntentService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(Constants.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, location);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        startService(intent);
    }



    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View view) {
        if(view==mRelLytAU){
            ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                    .edit().putString("country","au").commit();
            ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                    .edit().putString("telephone","+61 2 9264 5296").commit();
            startActivity(new Intent(ActivitySplash.this, ActivityHome.class));
            finish();
        }if(view==mRelLytIn){
            ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                    .edit().putString("country","in").commit();
            //+91 172 4016414
            ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                    .edit().putString("telephone","+91 172 4016414").commit();
            startActivity(new Intent(ActivitySplash.this, ActivityHome.class));
            finish();

        }if(view==mRelLytOT){
            ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                    .edit().putString("country","in").commit();
            //+91 172 4016414
            ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                    .edit().putString("telephone","+91 172 4016414").commit();
            startActivity(new Intent(ActivitySplash.this, ActivityHome.class));
            finish();
        }
    }


    private class SplashAsync extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(Void v){
            startActivity(new Intent(ActivitySplash.this,ActivityHome.class));
            finish();
        }


    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        updateUI();
                        break;
                }
                break;
        }
    }






    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         *  Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
           String  mAddressOutput = resultData.getString(Constants.RESULT_DATA_KEY);
            if(mAddressOutput.equalsIgnoreCase("IN")){
                ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                        .edit().putString("country","in").commit();
                //+91 172 4016414
                ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                        .edit().putString("telephone","+91 172 4016414").commit();

            } if(mAddressOutput.equalsIgnoreCase("AU")){
                ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                        .edit().putString("country","au").commit();
                ActivitySplash.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                        .edit().putString("telephone","+61 2 9264 5296").commit();

            }

            mProgressBar.setVisibility(View.GONE);
            startActivity(new Intent(ActivitySplash.this, ActivityHome.class));
            finish();
           //displayAddressOutput();

            // Show a toast message if an address was found.
            if (resultCode == Constants.SUCCESS_RESULT) {
              //  showToast(getString(R.string.address_found));
            }

            // Reset. Enable the Fetch Address button and stop showing the progress bar.
            //mAddressRequested = false;
            //updateUIWidgets();
        }
    }

}
