package com.asiapacificgroup.modals;

/**
 * Created by manishautomatic on 07/02/17.
 */

public class BlogTemplate {

    private String bgImgUrl="";
    private String smallTitle="";
    private String linkURL="";


    public String getBgImgUrl() {
        return bgImgUrl;
    }

    public void setBgImgUrl(String bgImgUrl) {
        this.bgImgUrl = bgImgUrl;
    }

    public String getSmallTitle() {
        return smallTitle;
    }

    public void setSmallTitle(String smallTitle) {
        this.smallTitle = smallTitle;
    }

    public String getLinkURL() {
        return linkURL;
    }

    public void setLinkURL(String linkURL) {
        this.linkURL = linkURL;
    }
}
