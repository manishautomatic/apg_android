package com.asiapacificgroup.modals;

/**
 * Created by manishautomatic on 25/03/17.
 */
public class GetBannersResponseTemplate {

    private String response_code="";
    private String response_message="";
    private BannersTemplate[] data;


    public String getResponse_code() {
        return response_code;
    }

    public void setResponse_code(String response_code) {
        this.response_code = response_code;
    }

    public BannersTemplate[] getData() {
        return data;
    }

    public void setData(BannersTemplate[] data) {
        this.data = data;
    }

    public String getResponse_message() {
        return response_message;
    }

    public void setResponse_message(String response_message) {
        this.response_message = response_message;
    }
}
