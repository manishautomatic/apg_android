package com.asiapacificgroup.modals;

/**
 * Created by manishautomatic on 25/03/17.
 */
public class BannersTemplate {

    private String banner_url="";
    private String banner_image="";


    public String getBanner_url() {
        return banner_url;
    }

    public void setBanner_url(String banner_url) {
        this.banner_url = banner_url;
    }

    public String getBanner_image() {
        return banner_image;
    }

    public void setBanner_image(String banner_image) {
        this.banner_image = banner_image;
    }
}
