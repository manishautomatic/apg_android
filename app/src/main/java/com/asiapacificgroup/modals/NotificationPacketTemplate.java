package com.asiapacificgroup.modals;

/**
 * Created by manishautomatic on 14/10/16.
 */
public class NotificationPacketTemplate {
    private String title ="";
    private String message="";
    private String url="";
    private String image="";


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
