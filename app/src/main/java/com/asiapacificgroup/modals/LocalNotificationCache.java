package com.asiapacificgroup.modals;

import java.util.ArrayList;

/**
 * Created by manishautomatic on 26/03/17.
 */
public class LocalNotificationCache {

    ArrayList<NotificationPacketTemplate> wrapper = new ArrayList<>();


    public ArrayList<NotificationPacketTemplate> getTemplates() {
        return wrapper;
    }

    public void setTemplates(ArrayList<NotificationPacketTemplate> templates) {
        this.wrapper = templates;
    }
}
