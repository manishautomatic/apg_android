package com.asiapacificgroup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.modals.LocalNotificationCache;
import com.asiapacificgroup.modals.NotificationPacketTemplate;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by manishautomatic on 08/03/17.
 */
public class ActivityNotifications  extends AppCompatActivity implements View.OnClickListener{


    private RelativeLayout mRelLytSearch;
    private LinearLayout mLinLytWrapper;
    private LayoutInflater mInflator;
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE=8;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        overridePendingTransition(R.anim.slide_in_from_below, R.anim.slide_in_right);
        setContentView(R.layout.activity_notifications);
        prepareFooterViews();;
        initUI();
        populateNotifications();
        getApplicationContext()
                .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                .edit()
                .putString("count_notif", "0")
                .commit();


    }

    @Override
    public void onClick(View view) {

    }

    private void initUI(){
        mLinLytWrapper=(LinearLayout)findViewById(R.id.linLytWrapper);
        mInflator=LayoutInflater.from(ActivityNotifications.this);
    }


    private void populateNotifications(){

        try{
            String storedNotification= getApplicationContext()
                    .getSharedPreferences("APP_PREFS",Context.MODE_PRIVATE)
                    .getString("local_notifs","");
            Gson gson = new Gson();
            LocalNotificationCache lCache_ = (LocalNotificationCache)
                    gson.fromJson(storedNotification,LocalNotificationCache.class);
            ArrayList<NotificationPacketTemplate> notificationList = new ArrayList();

                notificationList.clear();;
            notificationList.addAll(lCache_.getTemplates());
            Collections.reverse(notificationList);

            // now populate the list...

            for(final NotificationPacketTemplate notification :notificationList){
                View notificationView=new View(ActivityNotifications.this);
                    notificationView = mInflator.inflate(R.layout.notification_list_template,null);
                TextView title = (TextView)notificationView.findViewById(R.id.txtvwNotificationTitle);
                TextView Description = (TextView)notificationView.findViewById(R.id.txtvwNotificationDescription);
                ImageView mIgVwImage = (ImageView)notificationView.findViewById(R.id.imgVwNotificationImage);
                title.setText(notification.getTitle());
                Description.setText(notification.getMessage());
                String url = notification.getImage();
                url = url.replaceAll("\\.\\.","");
                url= ApgAppInstance.BASE_IMAGE+url;
                Picasso.with(ActivityNotifications.this)
                        .load(url)
                        .placeholder(R.drawable.apsgroup_)
                        .error(R.drawable.apsgroup_)
                        .into(mIgVwImage);
                mIgVwImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ApgAppInstance.PAGE_TO_LOAD = notification.getUrl();
                        ;

                        Intent intent = new Intent(ActivityNotifications.this, ActivityLocalWebContent.class);
                        startActivity(intent);
                    }
                });

                mLinLytWrapper.addView(notificationView);
            }
        }catch (Exception e){

        }

    }


    private void prepareFooterViews(){
        LinearLayout lytDialier = (LinearLayout)findViewById(R.id.initCall);
        LinearLayout lytEnqury = (LinearLayout)findViewById(R.id.initEnquiry);

        TextView txtvwDialer = (TextView)findViewById(R.id.txtvwDialer);
        txtvwDialer.setText("Call:" + ActivityNotifications.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", ""));

        lytDialier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if permission to make calls has been granted..
                if (ContextCompat.checkSelfPermission(ActivityNotifications.this,
                        android.Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityNotifications.this,
                            android.Manifest.permission.CALL_PHONE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(ActivityNotifications.this,
                                new String[]{android.Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_CALL_PHONE);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }else{
                    // permission is available, initiate call action.
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + ActivityNotifications.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", "")));
                    startActivity(intent);
                }
            }
        });
        lytEnqury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApgAppInstance.PAGE_TO_LOAD=ApgAppInstance.ENQUIRY_LINK;
                Intent intent = new Intent(ActivityNotifications.this,ActivityLocalWebContent.class);
                startActivity(intent);

            }
        });
    }


    private class ViewHolder{
        TextView mTxtVwTitle;
        ImageView mIngVwImage;
        TextView mTxtVwDescription;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
