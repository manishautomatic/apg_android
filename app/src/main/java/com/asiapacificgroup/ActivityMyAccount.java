package com.asiapacificgroup;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.modals.SignUpResponseTemplate;
import com.asiapacificgroup.modals.UpdatePasswordTemplate;
import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by manishautomatic on 08/03/17.
 */
public class ActivityMyAccount extends AppCompatActivity implements View.OnClickListener{


    private RelativeLayout mRelLytChangePassword,mRelLytSignOut;
    private EditText mEdtxtOldPassword,mEdTxtPassword,mEdtxtConfirmPassword;
    private TextView mTxtVwName, mTxtVwEmail, mTxtVwDob;
    private ProgressDialog mProgressBar;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        overridePendingTransition(R.anim.slide_in_from_below, R.anim.slide_in_right);
        setContentView(R.layout.activity_my_account);
        initUI();

    }

    private void initUI(){
        mProgressBar=new ProgressDialog(ActivityMyAccount.this);
        mProgressBar.setMessage("Updating password, please wait...");
        mTxtVwName=(TextView)findViewById(R.id.txtvwUserName);
        mTxtVwDob=(TextView)findViewById(R.id.txtvwuserDob);
        mTxtVwEmail=(TextView)findViewById(R.id.txtvwuserEmail);
        //

        SharedPreferences sharedPreferences = ActivityMyAccount.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE);

        mTxtVwName.setText(sharedPreferences.getString("user_name",""));
        mTxtVwDob.setText(sharedPreferences.getString("user_dob",""));
        mTxtVwEmail.setText(sharedPreferences.getString("user_email",""));

        mEdtxtOldPassword=(EditText)findViewById(R.id.edtxtCurrentPassword);
        mEdTxtPassword=(EditText)findViewById(R.id.edtxtNewPassword);
        mEdtxtConfirmPassword=(EditText)findViewById(R.id.edtxtConfirmPassword);
        //
        mRelLytChangePassword=(RelativeLayout)findViewById(R.id.rellytChangePassword);
        mRelLytSignOut=(RelativeLayout)findViewById(R.id.rellytSignOut);
        mRelLytChangePassword.setOnClickListener(this);
        mRelLytSignOut.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view==mRelLytChangePassword){
                changePassword();
        }if(view==mRelLytSignOut){
                performSignOut();
        }
    }




    // change the password of the user...

    private void changePassword(){
        final String currentPassword = mEdtxtOldPassword.getText().toString();
        if(currentPassword==null || currentPassword.trim().equalsIgnoreCase("")){
            Toast.makeText(ActivityMyAccount.this,
                    "please provide your current password", Toast.LENGTH_SHORT).show();
            return;
        }
        String newPassword = mEdTxtPassword.getText().toString();
        if(newPassword==null || newPassword.trim().equalsIgnoreCase("")){
            Toast.makeText(ActivityMyAccount.this,
                    "please provide new password", Toast.LENGTH_SHORT).show();
            return;
        }
        final String confirmPassword = mEdtxtConfirmPassword.getText().toString();
        if(!confirmPassword.equalsIgnoreCase(newPassword)){
            Toast.makeText(ActivityMyAccount.this,
                    "new password and confirm password do not match", Toast.LENGTH_SHORT).show();
            return;
        }
        // now initiate the password change process
            mProgressBar.show();
        StringRequest sr = new StringRequest(Request.Method.POST, ApgAppInstance.EP_UPDATE_PASSWORD ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("", response.toString());
                        Gson gson = new Gson();
                        UpdatePasswordTemplate updatePasswordTemplate = (UpdatePasswordTemplate)
                                gson.fromJson(response,
                                        UpdatePasswordTemplate.class);

                        if(updatePasswordTemplate.getRespononse_code().equalsIgnoreCase("1")){

                            new AlertDialog.Builder(ActivityMyAccount.this)
                                    .setTitle("Asia Pacific Group")
                                    .setMessage("Password changed successfully, please re login using your new password")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            ActivityMyAccount.this
                                                    .getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                                                    .edit()
                                                    .putString("user_id","")
                                                    .commit();
                                            startActivity(new Intent(ActivityMyAccount.this, ActivityHome.class));
                                            finish();
                                        }
                                    }).show();
                        }else{
                            new AlertDialog.Builder(ActivityMyAccount.this)
                                    .setTitle("Asia Pacific Group")
                                    .setMessage(updatePasswordTemplate.getResponse_message())
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    }).show();
                        }

                        mProgressBar.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ActivityMyAccount.this,"could not modify password, please try again...",Toast.LENGTH_LONG).show();
                VolleyLog.d("", "Error: " + error.getMessage());
                Log.d("", ""+error.getMessage()+","+error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                String user_id = ActivityMyAccount.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                        .getString("user_id","");
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("password", md5(currentPassword));
                params.put("new_password", md5(confirmPassword));
                return params;
            }
        };
        sr.setShouldCache(false);
        ApgAppInstance.getAppInstance().addToRequestQueue(sr, "change_password");

    }


    // signout the currently logged in user..

    private void performSignOut(){
        // reconfirm the signout action by the user...
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Asia Pacific Group");
        alertDialog.setMessage("Are you sure you want to logout?");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ActivityMyAccount.this
                        .getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                        .edit()
                        .putString("user_id","")
                        .commit();
                startActivity(new Intent(ActivityMyAccount.this, ActivityHome.class));
                finish();
            }
        });
        alertDialog.setButton2("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Toast.makeText(getApplicationContext(), "yoy have pressed cancel", 1).show();
            }
        });
        // Set the Icon for the Dialog
        alertDialog.setIcon(R.drawable.site_logo);
        alertDialog.show();
    }

    private static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


}
