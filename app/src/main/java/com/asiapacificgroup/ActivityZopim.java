package com.asiapacificgroup;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.controller.JsInterface;

/**
 * Created by manishautomatic on 22/02/17.
 */

public class ActivityZopim extends AppCompatActivity {

    private WebView mWebView;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        getSupportActionBar().hide();;
        overridePendingTransition(R.anim.slide_in_from_below, R.anim.slide_in_right);

        setContentView(R.layout.activity_local_web_content);
        initUI();
    }

    private void initUI() {
        mWebView = (WebView) findViewById(R.id.wvLocalContent);
        JsInterface jsInterface = new JsInterface(this);


        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.addJavascriptInterface(jsInterface, "JsInterface");
        mWebView.loadUrl("https://zendesk.onelogin.com/login");
        prepareFooterViews();
    }


    private void prepareFooterViews(){
        LinearLayout lytDialier = (LinearLayout)findViewById(R.id.initCall);
        LinearLayout lytEnqury = (LinearLayout)findViewById(R.id.initEnquiry);

        lytDialier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + ApgAppInstance.PHONE_NUMBER));
                startActivity(intent);
            }
        });
        lytEnqury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApgAppInstance.PAGE_TO_LOAD = ApgAppInstance.ENQUIRY_LINK;
                Intent intent = new Intent(ActivityZopim.this, ActivityLocalWebContent.class);
                startActivity(intent);

            }
        });
    }


}
