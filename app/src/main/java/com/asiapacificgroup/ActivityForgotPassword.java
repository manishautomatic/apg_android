package com.asiapacificgroup;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.modals.LoginResponseTemplate;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by manishautomatic on 07/02/17.
 */
public class ActivityForgotPassword extends AppCompatActivity implements View.OnClickListener {

    private EditText mEdtxtForgotPassword;
    private RelativeLayout mRelLytRequestReset;
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE=8;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        overridePendingTransition(R.anim.slide_in_from_below, R.anim.slide_in_right);
        setContentView(R.layout.activity_forgot_password);
        prepareFooterViews();
        initUI();

    }



    private void initUI(){

        mRelLytRequestReset=(RelativeLayout)findViewById(R.id.rellytForgotPassword);
        mRelLytRequestReset.setOnClickListener(this);

        mEdtxtForgotPassword=(EditText)findViewById(R.id.edtxtEmail);

    }

    private void prepareFooterViews(){
        LinearLayout lytDialier = (LinearLayout)findViewById(R.id.initCall);
        LinearLayout lytEnqury = (LinearLayout)findViewById(R.id.initEnquiry);
        TextView txtvwDialer = (TextView)findViewById(R.id.txtvwDialer);
        txtvwDialer.setText("Call:" + ActivityForgotPassword.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", ""));

        lytDialier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if permission to make calls has been granted..
                if (ContextCompat.checkSelfPermission(ActivityForgotPassword.this,
                        android.Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityForgotPassword.this,
                            android.Manifest.permission.CALL_PHONE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(ActivityForgotPassword.this,
                                new String[]{android.Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_CALL_PHONE);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }else{
                    // permission is available, initiate call action.
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + ActivityForgotPassword.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", "")));
                    startActivity(intent);
                }
            }
        });
        lytEnqury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApgAppInstance.PAGE_TO_LOAD = ApgAppInstance.ENQUIRY_LINK;
                Intent intent = new Intent(ActivityForgotPassword.this, ActivityLocalWebContent.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == mRelLytRequestReset) {
            requestPasswordReset();
        }
    }



        private void requestPasswordReset(){

           final ProgressDialog pDialog = new ProgressDialog(ActivityForgotPassword.this);
            pDialog.setMessage("Please wait...");

            final String email = mEdtxtForgotPassword.getText().toString();
            if(!isValidEmail(email)){
                Toast.makeText(ActivityForgotPassword.this, "Please provide valid email", Toast.LENGTH_LONG).show();
                return;
            }


            // now perform the server I/O
            pDialog.setMessage("Loggin in, Please wait...");
            StringRequest sr = new StringRequest(Request.Method.POST, ApgAppInstance.EP_PASSWORD_RESET,
                    new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("", response.toString());
                    Gson gson = new Gson();
                    LoginResponseTemplate loginResponse = (LoginResponseTemplate)gson.fromJson(response
                            ,LoginResponseTemplate.class);

                    if(loginResponse.getResponse_code().equalsIgnoreCase("1")){

                        new AlertDialog.Builder(ActivityForgotPassword.this)
                                .setTitle("Asia Pacific Group")
                                .setMessage(loginResponse.getResponse_message())
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                }).show();


                    }else{

                        new AlertDialog.Builder(ActivityForgotPassword.this)
                                .setTitle("Asia Pacific Group")
                                .setMessage(loginResponse.getResponse_message())
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                }).show();
                    }
                    pDialog.dismiss();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    new AlertDialog.Builder(ActivityForgotPassword.this)
                            .setTitle("Asia Pacific Group")
                            .setMessage("Could not complete your request.")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();
                    VolleyLog.d("", "Error: " + error.getMessage());
                    Log.d("", ""+error.getMessage()+","+error.toString());
                }
            }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", email);

                    return params;
                }
            };
            sr.setShouldCache(false);
            ApgAppInstance.getAppInstance().addToRequestQueue(sr, "login");
         }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
