package com.asiapacificgroup.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.asiapacificgroup.ActivityHome;
import com.asiapacificgroup.ActivityLocalWebContent;
import com.asiapacificgroup.R;
import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.modals.BlogTemplate;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by manishautomatic on 07/02/17.
 */

public class BlogsPagerAdapter extends PagerAdapter {

        private ArrayList<BlogTemplate> topBlogs = new ArrayList<>();
        private Context context;
        private LayoutInflater mLayoutInflater;

        public BlogsPagerAdapter(Context parentReference, ArrayList<BlogTemplate>blogs){
            this.context=parentReference;
            mLayoutInflater=LayoutInflater.from(parentReference);
            topBlogs.clear();
            topBlogs.addAll(blogs);
        }

    @Override
    public int getCount() {
        return topBlogs.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.layout_blogs_pager, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imgvwBlogFeatureImage);
            //imageView.setImageResource(R.drawable.intro_banner);
        TextView txtvw = (TextView) itemView.findViewById(R.id.txtvwBlogTitle);

        //imageView.setImageResource(mResources[position]);
        String url = topBlogs.get(position).getBgImgUrl();
        url = url.replaceAll("\\.\\.","");
        url= ApgAppInstance.BASE_IMAGE+url;
        Picasso.with(context)
                .load(url)
                .placeholder(R.drawable.apsgroup_)
                .error(R.drawable.apsgroup_)
                .into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApgAppInstance.PAGE_TO_LOAD=topBlogs.get(position).getLinkURL();

                Intent intent = new Intent(context,ActivityLocalWebContent.class);
                ((Activity)context).startActivity(intent);
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
