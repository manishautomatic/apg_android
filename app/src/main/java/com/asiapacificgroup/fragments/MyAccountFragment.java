package com.asiapacificgroup.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.asiapacificgroup.ActivityAgentLogin;
import com.asiapacificgroup.ActivityDashboard;
import com.asiapacificgroup.ActivityForgotPassword;
import com.asiapacificgroup.ActivitySignUp;
import com.asiapacificgroup.R;

/**
 * Created by manishautomatic on 06/02/17.
 */

public class MyAccountFragment extends Fragment implements View.OnClickListener {

    private RelativeLayout mRelLytSignUp;
    private RelativeLayout mRelLytAgentLogin;
    private RelativeLayout mRelLytContinue;
    private TextView mTxtVwForgotPassword;
    private ImageView mImgVwFBLogin;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_account, container, false);
        initUI(view);
        return view;
    }



    private void initUI(View view){
        mRelLytSignUp=(RelativeLayout)view.findViewById(R.id.rellytSignUp);
        mRelLytAgentLogin=(RelativeLayout)view.findViewById(R.id.rellytagentLogin);
        mRelLytContinue=(RelativeLayout)view.findViewById(R.id.rellytLogin);
        mRelLytSignUp.setOnClickListener(this);
        mRelLytAgentLogin.setOnClickListener(this);
        mRelLytContinue.setOnClickListener(this);
        mTxtVwForgotPassword=(TextView)view.findViewById(R.id.txtvwForgotPassword);
        mTxtVwForgotPassword.setOnClickListener(this);
        mImgVwFBLogin=(ImageView)view.findViewById(R.id.btnFbLogin);
        mImgVwFBLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
            if(view==mRelLytSignUp){
                getActivity().startActivity(new Intent(getActivity(), ActivitySignUp.class));
            }if(view==mRelLytAgentLogin){
            getActivity().startActivity(new Intent(getActivity(), ActivityAgentLogin.class));
        }if(view==mTxtVwForgotPassword){
            getActivity().startActivity(new Intent(getActivity(), ActivityForgotPassword.class));
        }if(view==mRelLytContinue){
            getActivity().startActivity(new Intent(getActivity(), ActivityDashboard.class));
        }if(view==mImgVwFBLogin){
            getActivity().startActivity(new Intent(getActivity(), ActivityDashboard.class));
        }
    }
}
