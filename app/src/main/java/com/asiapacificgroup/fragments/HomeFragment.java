package com.asiapacificgroup.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.asiapacificgroup.ActivityAgentLogin;
import com.asiapacificgroup.ActivityDashboard;
import com.asiapacificgroup.ActivityForgotPassword;
import com.asiapacificgroup.ActivityLocalWebContent;
import com.asiapacificgroup.ActivityMakeApplication;
import com.asiapacificgroup.ActivityMyAccount;
import com.asiapacificgroup.ActivityServicesOffered;
import com.asiapacificgroup.ActivitySignUp;
import com.asiapacificgroup.ActivitySplash;
import com.asiapacificgroup.ActivityTrackApplication;
import com.asiapacificgroup.R;
import com.asiapacificgroup.adapters.BlogsPagerAdapter;
import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.controller.JsInterface;
import com.asiapacificgroup.modals.BannersTemplate;
import com.asiapacificgroup.modals.BlogTemplate;
import com.asiapacificgroup.modals.GetBannersResponseTemplate;
import com.asiapacificgroup.modals.LoginResponseTemplate;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by manishautomatic on 06/02/17.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {

    private ViewPager mPagerBlogs;
    private BlogsPagerAdapter blogsPagerAdapter;
    //private WebView mWebViewHomePageLists;
    private RelativeLayout mRelLytSignUp;
    private RelativeLayout mRelLytAgentLogin;
    private RelativeLayout mRelLytContinue;
    private TextView mTxtVwForgotPassword;
    private ImageView mImgVwFBLogin;
    private RelativeLayout mRelLytLoginControls;
    private LinearLayout mLinLytDashboardControls;
    private RelativeLayout mRelLytMakeApplication,
                         mRelLytTrackApplication,
                         mRelLytServicesOffered,
                         mRelLytMyAccount,mRelLytPTE,mRelLytOffers;
    // login  controls
    private EditText mEdTxtEmail;
    private EditText mEdtTxtPassword;
    private RelativeLayout mRellytLogin;
    private RelativeLayout mRellytAgentLogin;
    private ProgressDialog pDialog;
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE=8;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View currentView =inflater.inflate(R.layout.fragment_home, container, false);
            initCurrentView(currentView);
        initUI();
        handlerPageChangeListner();
        prepareWebView();
        prepareFooterViews(currentView);
        fetchPagerBanners();
        return currentView;
    }


    private void prepareFooterViews(View view){
                LinearLayout lytDialier = (LinearLayout)view.findViewById(R.id.initCall);
        LinearLayout lytEnqury = (LinearLayout)view.findViewById(R.id.initEnquiry);
        TextView txtvwDialer = (TextView)view.findViewById(R.id.txtvwDialer);
        txtvwDialer.setText("Call:" + getActivity().getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", ""));

        lytDialier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if permission to make calls has been granted..
                if (ContextCompat.checkSelfPermission(getActivity(),
                        android.Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            android.Manifest.permission.CALL_PHONE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{android.Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_CALL_PHONE);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }else{
                    // permission is available, initiate call action.
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + getActivity().getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", "")));
                    startActivity(intent);
                }
            }
        });
        lytEnqury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApgAppInstance.PAGE_TO_LOAD = ApgAppInstance.ENQUIRY_LINK;
                Intent intent = new Intent(getActivity(), ActivityLocalWebContent.class);
                getActivity().startActivity(intent);

            }
        });
    }



    private void initCurrentView(View view){
            mPagerBlogs=(ViewPager)view.findViewById(R.id.pagerBlogs);
           // mWebViewHomePageLists=(WebView)view.findViewById(R.id.wvListSelector);
        ImageView dotOne = (ImageView) view.findViewById(R.id.imgpage1);
        dotOne.setBackgroundResource(R.drawable.dot_white);
        mRelLytSignUp=(RelativeLayout)view.findViewById(R.id.rellytSignUp);
        mRelLytAgentLogin=(RelativeLayout)view.findViewById(R.id.rellytagentLogin);
        mRelLytContinue=(RelativeLayout)view.findViewById(R.id.rellytLogin);
        mRelLytSignUp.setOnClickListener(this);
        mRelLytAgentLogin.setOnClickListener(this);
        mRelLytContinue.setOnClickListener(this);
        mTxtVwForgotPassword=(TextView)view.findViewById(R.id.txtvwForgotPassword);
        mTxtVwForgotPassword.setOnClickListener(this);
        mImgVwFBLogin=(ImageView)view.findViewById(R.id.btnFbLogin);
        mEdTxtEmail=(EditText) view.findViewById(R.id.edtxtEmail);
        mEdtTxtPassword=(EditText) view.findViewById(R.id.edtxtpassword);


        mImgVwFBLogin.setOnClickListener(this);
        mRelLytLoginControls=(RelativeLayout)view.findViewById(R.id.relLytLoginControls);
        // init the dashboard controls...
        mLinLytDashboardControls = (LinearLayout)view.findViewById(R.id.linlytDashboard);
        mRelLytMakeApplication = (RelativeLayout)view.findViewById(R.id.linlytMakeApplication);
        mRelLytTrackApplication = (RelativeLayout)view.findViewById(R.id.linlytTrackApplication);
        mRelLytServicesOffered = (RelativeLayout)view.findViewById(R.id.linlytServicesOffered);
        mRelLytMyAccount = (RelativeLayout)view.findViewById(R.id.linlytMyAccount);
        mRelLytPTE = (RelativeLayout)view.findViewById(R.id.rellytPTE);
        mRelLytOffers = (RelativeLayout)view.findViewById(R.id.rellytOffers);
        mRelLytMakeApplication.setOnClickListener(this);
        mRelLytTrackApplication.setOnClickListener(this);
        mRelLytServicesOffered.setOnClickListener(this);
        mRelLytMyAccount.setOnClickListener(this);
        mRelLytPTE.setOnClickListener(this);
        mRelLytOffers.setOnClickListener(this);
    }


    @Override
    public void onResume(){
        super.onResume();
        String user_id=getActivity()
                .getSharedPreferences("APP_PREFS", Activity.MODE_PRIVATE)
                .getString("user_id", "");
        if(user_id.equalsIgnoreCase("")){
            mRelLytLoginControls.setVisibility(View.VISIBLE);
            mLinLytDashboardControls.setVisibility(View.GONE);
        }else{
            mRelLytLoginControls.setVisibility(View.GONE);
            mLinLytDashboardControls.setVisibility(View.VISIBLE);
        }
    }

    private void initUI(){
        // temporarily add five blank blogs;//
        ArrayList<BlogTemplate> tempBlogs = new ArrayList<>();
            tempBlogs.add(new BlogTemplate());
            tempBlogs.add(new BlogTemplate());
            tempBlogs.add(new BlogTemplate());
            tempBlogs.add(new BlogTemplate());
            tempBlogs.add(new BlogTemplate());
        //blogsPagerAdapter=new BlogsPagerAdapter(getActivity(),tempBlogs);
        //mPagerBlogs.setAdapter(blogsPagerAdapter);

    }

    public void handlerPageChangeListner(){

        mPagerBlogs.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }



    private void prepareWebView(){
        JsInterface jsInterface = new JsInterface(getActivity());


       // mWebViewHomePageLists.getSettings().setJavaScriptEnabled(true);
        //mWebViewHomePageLists.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        //mWebViewHomePageLists.addJavascriptInterface(jsInterface, "JsInterface");
        String COUNTRY=getActivity().getSharedPreferences("APP_PREFS", Activity.MODE_PRIVATE).getString("country","");
        //mWebViewHomePageLists.setWebViewClient( new MyWebViewClient());
        if(COUNTRY.equalsIgnoreCase("in")){
           // mWebViewHomePageLists.loadUrl("file:///android_asset/webstatic/home.html");
          //  mWebViewHomePageLists.loadUrl("http://www.mobiprobe.com/apg/webstatic/in/home.html");
        }else{
            // mWebViewHomePageLists.loadUrl("file:///android_asset/webstatic-au/home.html");
            //mWebViewHomePageLists.loadUrl("http://www.mobiprobe.com/apg/webstatic/au/home.html");
        }

    }


    private void updateDots(int position){
        ImageView dotOne = (ImageView) getView().findViewById(R.id.imgpage1);
        ImageView dotTwo = (ImageView) getView().findViewById(R.id.imgpage2);
        ImageView dotThree = (ImageView) getView().findViewById(R.id.imgpage3);
        ImageView dotFour = (ImageView) getView().findViewById(R.id.imgpage4);
        ImageView dotFive = (ImageView) getView().findViewById(R.id.imgpage5);
        if(position==0){
            dotOne.setBackgroundResource(R.drawable.dot_white);
            dotTwo.setBackgroundResource(R.drawable.dot_grey);
            dotThree.setBackgroundResource(R.drawable.dot_grey);
            dotFour.setBackgroundResource(R.drawable.dot_grey);
            dotFive.setBackgroundResource(R.drawable.dot_grey);
        }if(position==1){
            dotOne.setBackgroundResource(R.drawable.dot_grey);
            dotTwo.setBackgroundResource(R.drawable.dot_white);
            dotThree.setBackgroundResource(R.drawable.dot_grey);
            dotFour.setBackgroundResource(R.drawable.dot_grey);
            dotFive.setBackgroundResource(R.drawable.dot_grey);
        }if(position==2){
            dotOne.setBackgroundResource(R.drawable.dot_grey);
            dotTwo.setBackgroundResource(R.drawable.dot_grey);
            dotThree.setBackgroundResource(R.drawable.dot_white);
            dotFour.setBackgroundResource(R.drawable.dot_grey);
            dotFive.setBackgroundResource(R.drawable.dot_grey);
        }if(position==3){
            dotOne.setBackgroundResource(R.drawable.dot_grey);
            dotTwo.setBackgroundResource(R.drawable.dot_grey);
            dotThree.setBackgroundResource(R.drawable.dot_grey);
            dotFour.setBackgroundResource(R.drawable.dot_white);
            dotFive.setBackgroundResource(R.drawable.dot_grey);
        }if(position==4){
            dotOne.setBackgroundResource(R.drawable.dot_grey);
            dotTwo.setBackgroundResource(R.drawable.dot_grey);
            dotThree.setBackgroundResource(R.drawable.dot_grey);
            dotFour.setBackgroundResource(R.drawable.dot_grey);
            dotFive.setBackgroundResource(R.drawable.dot_white);
        }

    }

    @Override
    public void onClick(View view) {
        if(view==mRelLytSignUp){
            getActivity().startActivity(new Intent(getActivity(), ActivitySignUp.class));
        }if(view==mRelLytAgentLogin){
            getActivity().startActivity(new Intent(getActivity(), ActivityAgentLogin.class));
        }if(view==mTxtVwForgotPassword){
            getActivity().startActivity(new Intent(getActivity(), ActivityForgotPassword.class));
        }if(view==mRelLytContinue){
           // getActivity().startActivity(new Intent(getActivity(), ActivityDashboard.class));
            doLogin();
        }if(view==mImgVwFBLogin){
           // getActivity().startActivity(new Intent(getActivity(), ActivityDashboard.class));
            toggleLoginControls();
        }if(view==mRelLytMakeApplication){
                   getActivity().startActivity(new Intent(getActivity(), ActivityMakeApplication.class));
        }if(view==mRelLytTrackApplication){
            getActivity().startActivity(new Intent(getActivity(), ActivityTrackApplication.class));
        }if(view==mRelLytServicesOffered){
            getActivity().startActivity(new Intent(getActivity(), ActivityServicesOffered.class));
        }if(view==mRelLytMyAccount){
            getActivity().startActivity(new Intent(getActivity(), ActivityMyAccount.class));
        }if(view==mRelLytPTE){
            ApgAppInstance.PAGE_TO_LOAD=ApgAppInstance.PTE_LINK;
            Intent intent = new Intent(getActivity(),ActivityLocalWebContent.class);
            getActivity().startActivity(intent);
            //getActivity().startActivity(new Intent(getActivity(), ActivityMyAccount.class));
        }if(view==mRelLytOffers){
            ApgAppInstance.PAGE_TO_LOAD=ApgAppInstance.OFFERS_LINK;
            Intent intent = new Intent(getActivity(),ActivityLocalWebContent.class);
            getActivity().startActivity(intent);
            //getActivity().startActivity(new Intent(getActivity(), ActivityMyAccount.class));
        }
    }


    // our custom webview client to control page navigations..

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return false;
        }
    }


    private void toggleLoginControls(){
            mRelLytLoginControls.setVisibility(View.GONE);
            mLinLytDashboardControls.setVisibility(View.VISIBLE);
    }

    // the web chrome client for handling the progress bar while the page is loading..

    private class MyWebChromeClient extends WebChromeClient{
            ProgressBar pBarReference;
        MyWebChromeClient(ProgressBar pBar){
                pBarReference=pBar;
        }

        @Override
        public void onProgressChanged(WebView view, int progress) {

            if(progress>90){
                if(this.pBarReference!=null)
                    pBarReference.setVisibility(View.GONE);
            }

        }
    }


    private void doLogin(){

        pDialog = new ProgressDialog(getActivity());

        final String email = mEdTxtEmail.getText().toString();
        if(!isValidEmail(email)){
            Toast.makeText(getActivity(),"Please provide valid email",Toast.LENGTH_LONG).show();
            return;
        }
        final String password = mEdtTxtPassword.getText().toString();
        if(password==null || password.trim()==""){
            Toast.makeText(getActivity(),"Please provide valid password",Toast.LENGTH_LONG).show();
            return;
        }

        // now perform the server I/O
        pDialog.setMessage("Loggin in, Please wait...");
        StringRequest sr = new StringRequest(Request.Method.POST, ApgAppInstance.EP_LOGIN , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("", response.toString());
                Gson gson = new Gson();
                LoginResponseTemplate loginResponse = (LoginResponseTemplate)gson.fromJson(response
                        ,LoginResponseTemplate.class);

                if(loginResponse.getResponse_code().equalsIgnoreCase("1")){
                    Toast.makeText(getActivity(),loginResponse.getResponse_message(),Toast.LENGTH_LONG).show();
                    getActivity()
                            .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                            .edit().putString("user_id", loginResponse.getUser_id())
                            .commit();
                    getActivity()
                            .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                            .edit().putString("user_email", loginResponse.getUser_email())
                            .commit();
                    getActivity()
                            .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                            .edit().putString("user_name", loginResponse.getUser_name())
                            .commit();
                    getActivity()
                            .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                            .edit().putString("user_dob", loginResponse.getUser_dob())
                            .commit();
                    mRelLytLoginControls.setVisibility(View.GONE);
                    mLinLytDashboardControls.setVisibility(View.VISIBLE);
                }else{
                    Toast.makeText(getActivity(),loginResponse.getResponse_message(),Toast.LENGTH_LONG).show();
                }
                pDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"could not login, please try again",Toast.LENGTH_LONG).show();
                VolleyLog.d("", "Error: " + error.getMessage());
                Log.d("", ""+error.getMessage()+","+error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", email);
                params.put("password", md5(password));
                params.put("type","1");
                return params;
            }
        };
        sr.setShouldCache(false);
        ApgAppInstance.getAppInstance().addToRequestQueue(sr,"login");
    }




        public final static boolean isValidEmail(CharSequence target) {
            if (target == null) {
                return false;
            } else {
                return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
            }
        }






    private static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
    //now we fetch the pager banners..

    private void fetchPagerBanners(){

        pDialog = new ProgressDialog(getActivity());



        // now perform the server I/O
        pDialog.setMessage("Loggin in, Please wait...");
        StringRequest sr = new StringRequest(Request.Method.POST, ApgAppInstance.EP_FETCH_BANNERS , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("", response.toString());
                Gson gson = new Gson();
                GetBannersResponseTemplate bannersList = (GetBannersResponseTemplate)gson.fromJson(response
                        ,GetBannersResponseTemplate.class);
                if(bannersList.getResponse_code().equalsIgnoreCase("1")){

                    prepareViewPager(bannersList.getData());

                }else{
                    Toast.makeText(getActivity(),bannersList.getResponse_message(),Toast.LENGTH_LONG).show();
                }
                pDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"could not fetch banners, please try again",Toast.LENGTH_LONG).show();
                VolleyLog.d("", "Error: " + error.getMessage());
                Log.d("", ""+error.getMessage()+","+error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                String userID = getActivity()
                                .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                                .getString("user_id", "");
                params.put("user_id", userID);
                return params;
            }
        };
        sr.setShouldCache(false);
        ApgAppInstance.getAppInstance().addToRequestQueue(sr,"fetch_banners");

    }



    private void prepareViewPager(BannersTemplate[] banners){
        ArrayList<BlogTemplate> tempBlogs = new ArrayList<>();
        for(BannersTemplate template:banners){
                BlogTemplate temp_b= new BlogTemplate();
            temp_b.setBgImgUrl(template.getBanner_image());
            temp_b.setLinkURL(template.getBanner_url());
            tempBlogs.add(temp_b);
        }

        blogsPagerAdapter=new BlogsPagerAdapter(getActivity(),tempBlogs);
        mPagerBlogs.setAdapter(blogsPagerAdapter);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }





}
