package com.asiapacificgroup;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.webkit.WebView;

import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.controller.JsInterface;

/**
 * Created by manishautomatic on 22/02/17.
 */

public class ActivitySubmitEnquiry extends AppCompatActivity {

    private WebView mWebView;
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE=8;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        getSupportActionBar().hide();
        overridePendingTransition(R.anim.slide_in_from_below, R.anim.slide_in_right);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_local_web_content);
        initUI();
    }

    private void initUI() {
        mWebView = (WebView) findViewById(R.id.wvLocalContent);
        JsInterface jsInterface = new JsInterface(this);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.addJavascriptInterface(jsInterface, "JsInterface");
        mWebView.loadUrl("");
    }


    @Override
    public void onBackPressed(){
            finish();
    }




    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }




}
