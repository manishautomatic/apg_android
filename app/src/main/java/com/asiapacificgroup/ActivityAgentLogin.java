package com.asiapacificgroup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.modals.LoginResponseTemplate;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by manishautomatic on 07/02/17.
 */
public class ActivityAgentLogin  extends AppCompatActivity implements View.OnClickListener {


    private EditText mEdTxtEmail;
    private EditText mEdtxtPassword;
    private RelativeLayout mRelLytLogin;
    private ProgressDialog pDialog;
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE=8;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        overridePendingTransition(R.anim.slide_in_from_below, R.anim.slide_in_right);
        setContentView(R.layout.activity_agent_login);
        initUI();
        prepareFooterViews();

    }



    private void initUI(){

        mEdTxtEmail = (EditText)findViewById(R.id.mEdtxtEmail);
        mEdtxtPassword = (EditText)findViewById(R.id.mEdtxtPassword);
        mRelLytLogin=(RelativeLayout)findViewById(R.id.rellytagentLogin);
        mRelLytLogin.setOnClickListener(this);
        pDialog=new ProgressDialog(this);

    }


    private void prepareFooterViews(){
        LinearLayout lytDialier = (LinearLayout)findViewById(R.id.initCall);
        LinearLayout lytEnqury = (LinearLayout)findViewById(R.id.initEnquiry);
        TextView txtvwDialer = (TextView)findViewById(R.id.txtvwDialer);
        txtvwDialer.setText("Call:" + ActivityAgentLogin.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", ""));

        lytDialier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if permission to make calls has been granted..
                if (ContextCompat.checkSelfPermission(ActivityAgentLogin.this,
                        android.Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityAgentLogin.this,
                            android.Manifest.permission.CALL_PHONE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(ActivityAgentLogin.this,
                                new String[]{android.Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_CALL_PHONE);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }else{
                    // permission is available, initiate call action.
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + ApgAppInstance.PHONE_NUMBER));
                    startActivity(intent);
                }

            }
        });
        lytEnqury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApgAppInstance.PAGE_TO_LOAD = ApgAppInstance.ENQUIRY_LINK;
                Intent intent = new Intent(ActivityAgentLogin.this, ActivityLocalWebContent.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view==mRelLytLogin){
            doLoginAction();
        }
    }



    private void doLoginAction(){
        doLogin();
    }


    private void doLogin(){



        final String email = mEdTxtEmail.getText().toString();
        if(!isValidEmail(email)){
            Toast.makeText(ActivityAgentLogin.this, "Please provide valid email", Toast.LENGTH_LONG).show();
            return;
        }
        final String password = mEdtxtPassword.getText().toString();
        if(password==null || password.trim()==""){
            Toast.makeText(ActivityAgentLogin.this,"Please provide valid password",Toast.LENGTH_LONG).show();
            return;
        }

        // now perform the server I/O
        pDialog.setMessage("Loggin in, Please wait...");
        StringRequest sr = new StringRequest(Request.Method.POST, ApgAppInstance.EP_LOGIN , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("", response.toString());
                Gson gson = new Gson();
                LoginResponseTemplate loginResponse = (LoginResponseTemplate)gson.fromJson(response
                        ,LoginResponseTemplate.class);

                if(loginResponse.getResponse_code().equalsIgnoreCase("1")){
                    Toast.makeText(ActivityAgentLogin.this,
                            loginResponse.getResponse_message(), Toast.LENGTH_LONG).show();
                    ActivityAgentLogin.this
                            .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                            .edit().putString("user_id", loginResponse.getUser_id())
                            .commit();
                    ActivityAgentLogin.this
                            .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                            .edit().putString("user_email", loginResponse.getUser_email())
                            .commit();
                    ActivityAgentLogin.this
                            .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                            .edit().putString("user_name", loginResponse.getUser_name())
                            .commit();
                    ActivityAgentLogin.this
                            .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                            .edit().putString("user_dob", loginResponse.getUser_dob())
                            .commit();

                }else{
                    Toast.makeText(ActivityAgentLogin.this,loginResponse.getResponse_message(),Toast.LENGTH_LONG).show();
                }
                pDialog.dismiss();
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ActivityAgentLogin.this,"could not login, please try again",Toast.LENGTH_LONG).show();
                VolleyLog.d("", "Error: " + error.getMessage());
                Log.d("", ""+error.getMessage()+","+error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", email);
                params.put("password", md5(password));
                params.put("type","2");
                return params;
            }
        };
        sr.setShouldCache(false);
        ApgAppInstance.getAppInstance().addToRequestQueue(sr, "login");
    }




    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }






    private static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }




    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
