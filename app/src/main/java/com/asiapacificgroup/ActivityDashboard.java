package com.asiapacificgroup;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by manishautomatic on 07/02/17.
 */
public class ActivityDashboard  extends AppCompatActivity implements View.OnClickListener {

private RelativeLayout mRelLytMakeApplication;
    private RelativeLayout mRelLytTrackApplication;
    private RelativeLayout mRelLytServicesOffered;
    private com.github.clans.fab.FloatingActionButton mFAB_Zopim, mFAB_Enquiry;
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE=8;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_from_below, R.anim.slide_in_right);
        setContentView(R.layout.activity_dashboard);
        initUI();

    }


    private void initUI(){
        /*
        mRelLytMakeApplication=(RelativeLayout)findViewById(R.id.rellytMakeApplication);
        mRelLytTrackApplication=(RelativeLayout)findViewById(R.id.rellytTrackApplication);
        mRelLytServicesOffered=(RelativeLayout)findViewById(R.id.rellytServicesOffered);
        mRelLytMakeApplication.setOnClickListener(this);
        mRelLytTrackApplication.setOnClickListener(this);
        mRelLytServicesOffered.setOnClickListener(this);
        mFAB_Enquiry=(com.github.clans.fab.FloatingActionButton)findViewById(R.id.menu_item);
        mFAB_Zopim=(com.github.clans.fab.FloatingActionButton)findViewById(R.id.menu_item_two);
        mFAB_Enquiry.setOnClickListener(this);
        mFAB_Zopim.setOnClickListener(this);
        */

    }


    @Override
    public void onClick(View view) {
        if(view==mRelLytMakeApplication){
            startActivity(new Intent(ActivityDashboard.this,ActivityMakeApplication.class));
        }if(view==mRelLytTrackApplication){
            startActivity(new Intent(ActivityDashboard.this,ActivityTrackApplication.class));
        }if(view==mRelLytServicesOffered){
            startActivity(new Intent(ActivityDashboard.this,ActivityServicesOffered.class));
        }if(view==mFAB_Enquiry){
            startActivity(new Intent(ActivityDashboard.this,ActivitySubmitEnquiry.class));
        }if(view==mFAB_Zopim){
            startActivity(new Intent(ActivityDashboard.this,ActivityZopim.class));
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
