package com.asiapacificgroup;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.modals.LoginResponseTemplate;
import com.asiapacificgroup.modals.SignUpResponseTemplate;
import com.google.gson.Gson;

import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by manishautomatic on 07/02/17.
 */
public class ActivitySignUp extends AppCompatActivity implements View.OnClickListener {


    private EditText mEdtxtName,mEdtxtEmail,mEdtxtMobile,mEdtxtPassword,mEdtxtConfirmPassword;
    private TextView mTxtVwDateOfBirth;
    Calendar calendar;
    boolean isOkayClicked = false;
    int byear, bmonth, bday;
    TextView tvSetDate;
    private RelativeLayout rellytSignUp;
    private ProgressDialog mProgressBar;
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE=8;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        overridePendingTransition(R.anim.slide_in_from_below, R.anim.slide_in_right);
        setContentView(R.layout.activity_signup);
        initUI();
        initCalendar();
        prepareFooterViews();
    }


    private void initUI(){
        mEdtxtName=(EditText)findViewById(R.id.edt_firstName);
        mEdtxtEmail=(EditText)findViewById(R.id.edt_email);
        mEdtxtMobile=(EditText)findViewById(R.id.edt_mobile_number);
        mEdtxtPassword=(EditText)findViewById(R.id.edt_password);
        mEdtxtConfirmPassword=(EditText)findViewById(R.id.edt_confirm_password);
        mTxtVwDateOfBirth=(TextView)findViewById(R.id.edt_dob);
        rellytSignUp=(RelativeLayout)findViewById(R.id.rellytSignUp);
        mProgressBar=new ProgressDialog(this);
        mProgressBar.setMessage("Signing up, please wait...");
        rellytSignUp.setOnClickListener(this);
        mTxtVwDateOfBirth.setOnClickListener(this);

    }

    private void initCalendar(){
        calendar = Calendar.getInstance();
        byear = calendar.get(Calendar.YEAR);
        bmonth = calendar.get(Calendar.MONTH);
        bday = calendar.get(Calendar.DAY_OF_MONTH);
    }

    private void prepareFooterViews(){
        LinearLayout lytDialier = (LinearLayout)findViewById(R.id.initCall);
        LinearLayout lytEnqury = (LinearLayout)findViewById(R.id.initEnquiry);

        TextView txtvwDialer = (TextView)findViewById(R.id.txtvwDialer);
        txtvwDialer.setText("Call:" + ActivitySignUp.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", ""));

        lytDialier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if permission to make calls has been granted..
                if (ContextCompat.checkSelfPermission(ActivitySignUp.this,
                        android.Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ActivitySignUp.this,
                            android.Manifest.permission.CALL_PHONE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(ActivitySignUp.this,
                                new String[]{android.Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_CALL_PHONE);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }else{
                    // permission is available, initiate call action.
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + ActivitySignUp.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", "")));
                    startActivity(intent);
                }
            }
        });
        lytEnqury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApgAppInstance.PAGE_TO_LOAD = ApgAppInstance.ENQUIRY_LINK;
                Intent intent = new Intent(ActivitySignUp.this, ActivityLocalWebContent.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view==mTxtVwDateOfBirth){
            showDateAlertDialog(mTxtVwDateOfBirth);
        }if(view==rellytSignUp){
            validateInputs();
        }
    }



    private void validateInputs(){
       final String name = mEdtxtName.getText().toString();
        if(name==null || name.trim().equalsIgnoreCase("")){
            Toast.makeText(ActivitySignUp.this,"Please provide your name",Toast.LENGTH_LONG).show();
            return;
        }
      final  String email = mEdtxtEmail.getText().toString();
        if(email==null || !isValidEmail(email)){
            Toast.makeText(ActivitySignUp.this,"Please provide valid email",Toast.LENGTH_LONG).show();
            return;
        }


      final  String dob = mTxtVwDateOfBirth.getText().toString();
        if(dob.trim().equalsIgnoreCase("")){
            Toast.makeText(ActivitySignUp.this,"Please provide date of birth",Toast.LENGTH_LONG).show();
            return;
        }

      final  String password=mEdtxtPassword.getText().toString();
        if(password==null || password.trim().length()<8){
            Toast.makeText(ActivitySignUp.this,"Please provide valid password",Toast.LENGTH_LONG).show();
            return;
        }
        String confirmPassword=mEdtxtConfirmPassword.getText().toString();
        if(!confirmPassword.equalsIgnoreCase(password)){
            Toast.makeText(ActivitySignUp.this,"Please provide valid password",Toast.LENGTH_LONG).show();
            return;
        }

        // now lets perform the signup process..
        mProgressBar.show();

        StringRequest sr = new StringRequest(Request.Method.POST, ApgAppInstance.EP_SIGNUP ,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("", response.toString());
                Gson gson = new Gson();
                SignUpResponseTemplate signUpResponseTemplate = (SignUpResponseTemplate)
                                                                gson.fromJson(response,
                                                                SignUpResponseTemplate.class);

                if(signUpResponseTemplate.getResponse_code().equalsIgnoreCase("1")){
                    ActivitySignUp.this
                            .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                            .edit().putString("user_id", signUpResponseTemplate.getUser_id())
                            .commit();
                    ActivitySignUp.this
                            .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                            .edit().putString("user_name", name)
                            .commit();
                    ActivitySignUp.this
                            .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                            .edit().putString("user_email", name)
                            .commit();

                    new AlertDialog.Builder(ActivitySignUp.this)
                            .setTitle("Asia Pacific Group")
                            .setMessage("Welcome "+name+" .Your sign up was successfull!")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(ActivitySignUp.this,ActivityHome.class));
                                    finish();
                                }
                            }).show();
                }else{
                    new AlertDialog.Builder(ActivitySignUp.this)
                            .setTitle("Asia Pacific Group")
                            .setMessage(signUpResponseTemplate.getResponse_message())
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(ActivitySignUp.this, ActivityHome.class));
                                    finish();
                                }
                            }).show();
                }

                mProgressBar.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ActivitySignUp.this,"could not complete signup, please try again",Toast.LENGTH_LONG).show();
                VolleyLog.d("", "Error: " + error.getMessage());
                Log.d("", ""+error.getMessage()+","+error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                String dob_=Integer.toString(byear)+"-"+Integer.toString(bmonth)+"-"+Integer.toString(bday);
                String user_id = ActivitySignUp.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                        .getString("user_id","");
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("password", md5(password));
                params.put("dob", dob_);
                params.put("email", email);

                ActivitySignUp.this
                        .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                        .edit().putString("user_dob", dob_)
                        .commit();

                return params;
            }

            /*
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<String, String>();
                headers.put("Content-Type","application/x-www-form-urlencoded");
                headers.put("abc", "value");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }*/
        };
        sr.setShouldCache(false);
        ApgAppInstance.getAppInstance().addToRequestQueue(sr, "file");

    }



    @SuppressLint("NewApi")
    private void showDateAlertDialog(TextView tvSetDate) {
        this.tvSetDate = tvSetDate;
        String date = "";
        final DateSetListener dataSetListener = new DateSetListener();
        final DatePickerDialog datePicker = new DatePickerDialog(ActivitySignUp.this,
                dataSetListener, byear, bmonth, bday);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            datePicker.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        } else {
            final int minYear = calendar.get(Calendar.YEAR);
            final int minMonth = calendar.get(Calendar.MONTH);
            final int minDay = calendar.get(Calendar.DAY_OF_MONTH);
            Field mDatePickerField = null;
            try {
                mDatePickerField = datePicker.getClass().getDeclaredField("mDatePicker");
            } catch (NoSuchFieldException e) {

                e.printStackTrace();
            }
            mDatePickerField.setAccessible(true);
            DatePicker mDatePickerInstance = null;
            try {
                mDatePickerInstance = (DatePicker) mDatePickerField.get(datePicker);
            } catch (IllegalArgumentException e) {

                e.printStackTrace();
            } catch (IllegalAccessException e) {

                e.printStackTrace();
            }

            mDatePickerInstance.init(minYear, minMonth, minDay, new DatePicker.OnDateChangedListener() {

                public void onDateChanged(DatePicker view, int year, int month, int day) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, month, day);
                    if (newDate.after(calendar)) {
                        view.init(minYear, minMonth, minDay, this);
                    }
                }
            });

        }
        datePicker.setButton(DialogInterface.BUTTON_NEGATIVE, ActivitySignUp.this
                .getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @SuppressLint("NewApi")
            public void onClick(DialogInterface dialog, int which) {
                isOkayClicked = false;

            }
        });
        datePicker.setButton(DialogInterface.BUTTON_POSITIVE,
                ActivitySignUp.this.getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
            @SuppressLint("NewApi")
            public void onClick(DialogInterface dialog, int which) {
                isOkayClicked = true;
                DatePicker datePickerDialog = datePicker.getDatePicker();
                dataSetListener.onDateSet(datePickerDialog, datePickerDialog.getYear(),
                        datePickerDialog.getMonth(), datePickerDialog.getDayOfMonth());
            }
        });
        datePicker.show();

    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private class DateSetListener implements DatePickerDialog.OnDateSetListener {

        public void onDateSet(DatePicker view, int year, int month, int day) {

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));

            byear = year;
            bmonth = month + 1;
            bday = day;
            Calendar c = Calendar.getInstance();
            c.set(year, month, day);
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String selectedDate = df.format(c.getTime());
            //Checking if OK clicked
            if (isOkayClicked) {
                tvSetDate.setText(selectedDate);
                isOkayClicked = false;
            }
            bmonth = month;
        }
    }



    private static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }




    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
