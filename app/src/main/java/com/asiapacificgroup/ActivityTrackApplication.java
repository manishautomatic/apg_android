package com.asiapacificgroup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.modals.LoginResponseTemplate;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by manishautomatic on 07/02/17.
 */
public class ActivityTrackApplication  extends AppCompatActivity  implements View.OnClickListener{


    private RelativeLayout mRelLytSearch;
    private EditText mEdTxtApplicationNumber;
    private ProgressDialog pDialog;
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE=8;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        overridePendingTransition(R.anim.slide_in_from_below, R.anim.slide_in_right);
        setContentView(R.layout.activity_track_application);
        mRelLytSearch=(RelativeLayout)findViewById(R.id.rellytSearhApplication);
        mRelLytSearch.setOnClickListener(this);
        mEdTxtApplicationNumber=(EditText)findViewById(R.id.edtxtApplicationNumber);
        prepareFooterViews();
        pDialog=new ProgressDialog(this);

    }

    @Override
    public void onClick(View view) {
        if(view==mRelLytSearch){
            trackApplication();
           // startActivity(new Intent(ActivityTrackApplication.this,ActivityApplicationStatus.class));
            //finish();
        }
    }

    private void prepareFooterViews(){
        LinearLayout lytDialier = (LinearLayout)findViewById(R.id.initCall);
        LinearLayout lytEnqury = (LinearLayout)findViewById(R.id.initEnquiry);

        TextView txtvwDialer = (TextView)findViewById(R.id.txtvwDialer);
        txtvwDialer.setText("Call:" + ActivityTrackApplication.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", ""));

        lytDialier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if permission to make calls has been granted..
                if (ContextCompat.checkSelfPermission(ActivityTrackApplication.this,
                        android.Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityTrackApplication.this,
                            android.Manifest.permission.CALL_PHONE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(ActivityTrackApplication.this,
                                new String[]{android.Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_CALL_PHONE);

                        // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }else{
                    // permission is available, initiate call action.
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + ActivityTrackApplication.this.getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE).getString("telephone", "")));
                    startActivity(intent);
                }
            }
        });
        lytEnqury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApgAppInstance.PAGE_TO_LOAD=ApgAppInstance.ENQUIRY_LINK;
                Intent intent = new Intent(ActivityTrackApplication.this,ActivityLocalWebContent.class);
                startActivity(intent);

            }
        });
    }



    private void trackApplication(){
       final String applicationNumber = mEdTxtApplicationNumber.getText().toString();
        if(applicationNumber==null||applicationNumber.trim().toString()==""){
            Toast.makeText(ActivityTrackApplication.this,"please provide application number",Toast.LENGTH_LONG).show();
            return;
        }


        StringRequest sr = new StringRequest(Request.Method.POST, ApgAppInstance.EP_TRACK_APPLICATION , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("", response.toString());
                Gson gson = new Gson();
                LoginResponseTemplate loginResponse = (LoginResponseTemplate)gson.fromJson(response
                        ,LoginResponseTemplate.class);

                if(loginResponse.getResponse_code().equalsIgnoreCase("1")){
                    //Toast.makeText(ActivityMakeApplication.this,loginResponse.getResponse_message(),Toast.LENGTH_LONG).show();
                    ApgAppInstance.APPICATION_NUMBER=loginResponse.getApplication_number();
                    ApgAppInstance.APPICANT_NAME=loginResponse.getApplicant_name();
                    ApgAppInstance.APPICANT_EMAIL=loginResponse.getApplicant_email();
                    ApgAppInstance.APPICATION_FILING_DATE=loginResponse.getApplication_filing_date();
                    ApgAppInstance.APPICATION_STATUS=loginResponse.getApplication_status();
                    ApgAppInstance.APPICATION_TYPE=loginResponse.getApplication_type();


                    startActivity(new Intent(ActivityTrackApplication.this,ActivityApplicationStatus.class));
                    finish();

                }else{
                    Toast.makeText(ActivityTrackApplication.this,loginResponse.getResponse_message(),Toast.LENGTH_LONG).show();
                }
                pDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ActivityTrackApplication.this,"could not find application, please try again",Toast.LENGTH_LONG).show();
                VolleyLog.d("", "Error: " + error.getMessage());
                Log.d("", ""+error.getMessage()+","+error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){

                String user_id = ActivityTrackApplication.this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                        .getString("user_id","");
                Map<String, String> params = new HashMap<String, String>();
                params.put("application_number", applicationNumber);
                params.put("user_id",user_id );
                return params;
            }

            /*
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<String, String>();
                headers.put("Content-Type","application/x-www-form-urlencoded");
                headers.put("abc", "value");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }*/
        };
        sr.setShouldCache(false);
        ApgAppInstance.getAppInstance().addToRequestQueue(sr, "file");
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
