package com.asiapacificgroup.controller;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.zopim.android.sdk.api.ZopimChat;

/**
 * Created by manishautomatic on 07/02/17.
 */

public class ApgAppInstance extends Application {

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static Context mContext;
    public static String APPICATION_NUMBER="";
    public static String APPICANT_NAME="";
    public static String APPICANT_EMAIL="";
    public static String APPICATION_FILING_DATE="";
    public static String APPICATION_STATUS="";
    public static String APPICATION_TYPE="";


    public static String PAGE_TO_LOAD="";
    public static String PTE_LINK="http://www.asiapacificgroup.com/ptevoucher/";
    public static String OFFERS_LINK="http://mobiprobe.com/apg/pages/public_offers.php";

    public static  String PHONE_NUMBER ="+61 2 9264 5296";
    public static String ENQUIRY_LINK = "http://www.mobiprobe.com/apg/pages/public_enquiry.php";
    private final static String BASE_URL="http://mobiprobe.com/apg/endpoint/";
    public final static String BASE_IMAGE="http://mobiprobe.com/apg";
    public final static String EP_LOGIN=BASE_URL+"publicLogin.php";
    public final static String EP_CREATE_APPLICATION=BASE_URL+"createapplication.php";
    public final static String EP_TRACK_APPLICATION=BASE_URL+"trackapplication.php";
    public final static String EP_SIGNUP=BASE_URL+"publicSignup.php";
    public final static String EP_UPDATE_PASSWORD=BASE_URL+"updatePassword.php";
    public final static String EP_FETCH_BANNERS=BASE_URL+"fetchPublicBanners.php";
    public final static String EP_SYNC_TOKEN=BASE_URL+"syncToken.php";
    public final static String EP_PASSWORD_RESET=BASE_URL+"passwordReset.php";

    private static ApgAppInstance appInstance=null;

    @Override
    public void onCreate(){
        super.onCreate();
        mContext=getApplicationContext();
        appInstance=this;
        ZopimChat.init("3KlgGzLKbCnQ7N7Ab5GUcCzdsW69YKPb");
        initVolley();
    }


    public static ApgAppInstance getAppInstance(){
        return appInstance;
    }


    private void initVolley(){
        mRequestQueue = getRequestQueue();
        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    public void updateToken(String token){
        if(token==null)return;

        this.getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                .edit()
                .putString("PUSH_TOKEN",token)
                .commit();
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(tag);
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
