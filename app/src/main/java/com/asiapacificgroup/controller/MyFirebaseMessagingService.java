package com.asiapacificgroup.controller;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.asiapacificgroup.ActivityNotifications;
import com.asiapacificgroup.R;
import com.asiapacificgroup.modals.LocalNotificationCache;
import com.asiapacificgroup.modals.NotificationPacketTemplate;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by manishautomatic on 11/10/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        //remoteMessage.getNotification().get\
        Map<String,String> remoteKeys =remoteMessage.getData();
                sendNotification(remoteKeys);
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(Map<String,String>keyset) {

        Gson gson = new Gson();
        NotificationPacketTemplate template = new NotificationPacketTemplate();
        try{
           // template = (NotificationPacketTemplate)gson.fromJson(messageBody,NotificationPacketTemplate.class);

            template =(NotificationPacketTemplate)gson.fromJson(keyset.get("data"),NotificationPacketTemplate.class);
            //AppInstance.getInstance().setCurrentNotificationTemplate(template);


        }catch (Exception e){

        }
            // now update the local cache of notification data and store in shared preference cache...
            try{

                String storedNotification= getApplicationContext()
                                            .getSharedPreferences("APP_PREFS",Context.MODE_PRIVATE)
                                            .getString("local_notifs","");
                if(storedNotification.equalsIgnoreCase("")){
                    ArrayList<NotificationPacketTemplate> lCache = new ArrayList<>();
                    lCache.add(template);
                    String cachedStr = gson.toJson(lCache);
                    cachedStr="{\"wrapper\":"+cachedStr+"}";
                    getApplicationContext()
                            .getSharedPreferences("APP_PREFS",Context.MODE_PRIVATE)
                            .edit()
                            .putString("local_notifs",cachedStr)
                            .commit();
                    getApplicationContext()
                            .getSharedPreferences("APP_PREFS",Context.MODE_PRIVATE)
                            .edit()
                            .putString("count_notif","1")
                            .commit();
                    Log.d("sender", "Broadcasting message");
                    Intent intent = new Intent("UPDATE_NOTIFICATION_COUNT");
                    // You can also include some extra data.
                    intent.putExtra("message", "This is my message!");
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

                }else{
                       // storedNotification="{\"wrapper\":"+storedNotification+"}";

                    LocalNotificationCache lCache_ = (LocalNotificationCache)
                                                    gson.fromJson(storedNotification,LocalNotificationCache.class);

                    lCache_.getTemplates().add(template);

                    String processedString = gson.toJson(lCache_);
                    getApplicationContext()
                            .getSharedPreferences("APP_PREFS",Context.MODE_PRIVATE)
                            .edit()
                            .putString("local_notifs",processedString)
                            .commit();

                    String countNotification = getApplicationContext()
                            .getSharedPreferences("APP_PREFS",Context.MODE_PRIVATE)
                            .getString("count_notif","1");
                    Integer _intCount = Integer.parseInt(countNotification);
                            _intCount=_intCount+1;
                    String _convertedString = Integer.toString(_intCount);
                    getApplicationContext()
                            .getSharedPreferences("APP_PREFS", Context.MODE_PRIVATE)
                            .edit()
                            .putString("count_notif", _convertedString)
                            .commit();

                    Log.d("sender", "Broadcasting message");
                    Intent intent = new Intent("UPDATE_NOTIFICATION_COUNT");
                    // You can also include some extra data.
                    intent.putExtra("message", "This is my message!");
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

                }

            }catch (Exception e){

            }




            Intent intent = new Intent(this, ActivityNotifications.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("url",template.getUrl());


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle("Asia Pacific Group")
                    .setContentText(template.getMessage())
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.site_logo);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());




    }



}
