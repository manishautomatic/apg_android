package com.asiapacificgroup.controller;

import android.app.Activity;
import android.content.Intent;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.asiapacificgroup.ActivityLocalWebContent;

/**
 * Created by manishautomatic on 07/02/17.
 */

public class JsInterface {

    private Activity activity;

    public JsInterface(Activity activiy) {
        this.activity = activiy;
    }

    @JavascriptInterface
    public void loadPage(String page){
        /*
        if(page.equalsIgnoreCase("pte.html")){
            ApgAppInstance.PAGE_TO_LOAD="http://www.asiapacificgroup.com/ptevoucher/";
        }else{
            ApgAppInstance.PAGE_TO_LOAD="file:///android_asset/webstatic/services.html";
        }
        */
        ApgAppInstance.PAGE_TO_LOAD=page;

        Intent intent = new Intent(activity,ActivityLocalWebContent.class);
        activity.startActivity(intent);
        //Toast.makeText(activity,page,Toast.LENGTH_LONG).show();
    }

    @JavascriptInterface
    public void dispatchNativ(String page){


        if(page.equalsIgnoreCase("pte.html")){
            ApgAppInstance.PAGE_TO_LOAD="http://www.asiapacificgroup.com/ptevoucher/";
        }if(page.equalsIgnoreCase("pte.html")){
            ApgAppInstance.PAGE_TO_LOAD="file:///android_asset/webstatic/services.html";
        }else{
            ApgAppInstance.PAGE_TO_LOAD="http://www.mobiprobe.com/apg/webstatic";
        }

        ApgAppInstance.PAGE_TO_LOAD=page;

        Intent intent = new Intent(activity,ActivityLocalWebContent.class);
        activity.startActivity(intent);
        //Toast.makeText(activity,page,Toast.LENGTH_LONG).show();
    }

    @JavascriptInterface
    public void finish(String page){
       if(activity!=null) activity.finish();

    }
}
