package com.asiapacificgroup.controller;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.asiapacificgroup.modals.LoginResponseTemplate;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by manishautomatic on 26/03/17.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
        //ApgAppInstance.getInstance().updateToken(refreshedToken);
        ApgAppInstance.getAppInstance().updateToken(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token) {
        if(token!=null && !token.trim().equalsIgnoreCase("")){
            // post it to server...

            StringRequest sr = new StringRequest(Request.Method.POST, ApgAppInstance.EP_SYNC_TOKEN , new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("", response.toString());
                    Gson gson = new Gson();
                    LoginResponseTemplate loginResponse = (LoginResponseTemplate)gson.fromJson(response
                            ,LoginResponseTemplate.class);

                    if(loginResponse.getResponse_code().equalsIgnoreCase("1")){

                    }else{

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //Toast.makeText(getActivity(),"could not login, please try again",Toast.LENGTH_LONG).show();
                    VolleyLog.d("", "Error: " + error.getMessage());
                    Log.d("", ""+error.getMessage()+","+error.toString());
                }
            }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("token", token);


                    return params;
                }
            };
            sr.setShouldCache(false);
            ApgAppInstance.getAppInstance().addToRequestQueue(sr,"token_sync");
        }

    }
}
