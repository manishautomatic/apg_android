package com.asiapacificgroup;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Point;
import android.media.Image;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.asiapacificgroup.controller.ApgAppInstance;
import com.asiapacificgroup.controller.JsInterface;
import com.asiapacificgroup.fragments.HomeFragment;
import com.asiapacificgroup.fragments.MyAccountFragment;
import com.github.clans.fab.FloatingActionMenu;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.zopim.android.sdk.prechat.ZopimChatActivity;

public class ActivityHome extends AppCompatActivity implements View.OnClickListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private ViewPagerAdapter mVwPagerAdapter;
    private Button mBtnHome, mBtnMyAccount;
    //private com.github.clans.fab.FloatingActionButton mFAB_Zopim, mFAB_Enquiry;
    //private FloatingActionMenu fam;
    private View behindMenuView, overlayView;
    private  ViewGroup decorRoot;
    int MENU_STATUS=0;// 0: MENU APPEAR, 1: MENU DISAPPEAR


    Animation menu_disappear;
    Animation menu_appear;
    Animation zoom_exit;
    Animation zoom_enter;
    ObjectAnimator moveLeft,moveDown,moveUp,moveRight,scaleX,scaleY,scaleBackX,scaleBackY;
    AnimatorSet animS;
    int screen_width=0;
    int screen_height;
    Point size = new Point();
    private FrameLayout.LayoutParams rootViewParams;
    private WebView mWebViewListing;
    private ImageView mImgVwToggleOptions;
    private ImageView mImgVwLaunchZopim, mImgVwNotifications;
    private ProgressBar mProgressBar;
    private TextView mTxtVwTicker;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setBackgroundColor(Color.WHITE);
       // getWindow().setWindowAnimations(android.R.anim.slide_in_left);
        overridePendingTransition(R.anim.anim_slide_out_left, 0);

        setContentView(R.layout.activity_home);

       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mVwPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mVwPagerAdapter);

       TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setVisibility(View.GONE);


        initUI();
        handleViewPagerPageChange();
        prepareAnimationObjects();
        prepareWebView();
        LocalBroadcastManager.getInstance(this).registerReceiver(performNearbySearchReceiver,
                new IntentFilter("UPDATE_NOTIFICATION_COUNT"));


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();



        LocalBroadcastManager.getInstance(this).unregisterReceiver(performNearbySearchReceiver);


    }

    private void prepareWebView(){
        JsInterface jsInterface = new JsInterface(ActivityHome.this);


        mWebViewListing.getSettings().setJavaScriptEnabled(true);
        mWebViewListing.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebViewListing.addJavascriptInterface(jsInterface, "JsInterface");
        String COUNTRY=ActivityHome.this.getSharedPreferences("APP_PREFS", Activity.MODE_PRIVATE).getString("country","");
        mWebViewListing.setWebViewClient( new MyWebViewClient());
        if(COUNTRY.equalsIgnoreCase("in")){
            // mWebViewHomePageLists.loadUrl("file:///android_asset/webstatic/home.html");
            mWebViewListing.loadUrl("http://www.mobiprobe.com/apg/webstatic/in/home.html");
        }else{
            // mWebViewHomePageLists.loadUrl("file:///android_asset/webstatic-au/home.html");
            mWebViewListing.loadUrl("http://www.mobiprobe.com/apg/webstatic/au/home.html");
        }

        mWebViewListing.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if(progress>80){
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });

    }


    private void prepareFooterViews(){
        LinearLayout lytDialier = (LinearLayout)findViewById(R.id.initCall);
        LinearLayout lytEnqury = (LinearLayout)findViewById(R.id.initEnquiry);

        lytDialier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + ApgAppInstance.PHONE_NUMBER));
                startActivity(intent);
            }
        });
        lytEnqury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApgAppInstance.PAGE_TO_LOAD = ApgAppInstance.ENQUIRY_LINK;
                Intent intent = new Intent(ActivityHome.this, ActivityLocalWebContent.class);
                startActivity(intent);

            }
        });
    }


    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return false;
        }
    }

    // the web chrome client for handling the progress bar while the page is loading..

    private class MyWebChromeClient extends WebChromeClient {
        ProgressBar pBarReference;
        MyWebChromeClient(ProgressBar pBar){
            pBarReference=pBar;
        }

        @Override
        public void onProgressChanged(WebView view, int progress) {

            if(progress>90){
                if(this.pBarReference!=null)
                    pBarReference.setVisibility(View.GONE);
            }

        }
    }

    private void initUI(){
        overlayView=(FrameLayout)findViewById(R.id.overlayView);
        ViewGroup dec= (ViewGroup)ActivityHome.this.getWindow().getDecorView();
        behindMenuView = LayoutInflater.from(ActivityHome.this).inflate(R.layout.behind_menu_layout, null);
        mWebViewListing = (WebView)behindMenuView.findViewById(R.id.webviewOptions);
        mProgressBar=(ProgressBar)behindMenuView.findViewById(R.id.ProgressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        decorRoot =(ViewGroup) dec.getChildAt(0);
        dec.removeView(decorRoot);
        dec.addView(behindMenuView);
        dec.addView(decorRoot);
        decorRoot.getChildCount();
        mImgVwToggleOptions=(ImageView)findViewById(R.id.imgVwToggleMenu);
        mImgVwToggleOptions.setOnClickListener(this);
        mImgVwLaunchZopim=(ImageView)findViewById(R.id.imgVwSupport);
        mImgVwLaunchZopim.setOnClickListener(this);
        mBtnHome=(Button)findViewById(R.id.mbtnHome);
        mBtnMyAccount=(Button)findViewById(R.id.mbtnMyAccount);
        mBtnMyAccount.setBackgroundResource(R.drawable.white_border);
        mBtnHome.setBackgroundResource(R.drawable.white_fill);
        mBtnMyAccount.setTextColor(Color.WHITE);
        mBtnHome.setTextColor(Color.BLACK);
        mImgVwNotifications=(ImageView)findViewById(R.id.imgVwShowNotification);
        mImgVwNotifications.setOnClickListener(this);
        mBtnMyAccount.setOnClickListener(this);
        mBtnHome.setOnClickListener(this);


        mTxtVwTicker=(TextView)findViewById(R.id.txtVwTickerCount);

    }


    private void handleViewPagerPageChange(){
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                    if(position==0){
                        mBtnHome.setBackgroundResource(R.drawable.white_fill);
                        mBtnHome.setTextColor(Color.BLACK);
                        mBtnMyAccount.setBackgroundResource(R.drawable.white_border);
                        mBtnMyAccount.setTextColor(Color.WHITE);


                    }if(position==1){
                    mBtnHome.setBackgroundResource(R.drawable.white_border);
                    mBtnHome.setTextColor(Color.WHITE);
                    mBtnMyAccount.setBackgroundResource(R.drawable.white_fill);
                    mBtnMyAccount.setTextColor(Color.BLACK);

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_activity_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if(view==mImgVwToggleOptions){
            showMenuList();
        }
        if(view==mBtnHome){

            /*
            mBtnHome.setBackgroundResource(R.drawable.white_fill);
            mBtnHome.setTextColor(Color.BLACK);
            mBtnMyAccount.setBackgroundResource(R.drawable.white_border);
            mBtnMyAccount.setTextColor(Color.WHITE);
            mViewPager.setCurrentItem(0);
            */

        }if(view==mBtnMyAccount){
            mBtnHome.setBackgroundResource(R.drawable.white_border);
            mBtnHome.setTextColor(Color.WHITE);
            mBtnMyAccount.setBackgroundResource(R.drawable.white_fill);
            mBtnMyAccount.setTextColor(Color.BLACK);
            mViewPager.setCurrentItem(1);
        }if(view==mImgVwNotifications){
            startActivity(new Intent(ActivityHome.this,ActivityNotifications.class));
        }if(view==mImgVwLaunchZopim){
            //startActivity(new Intent(ActivityHome.this,ActivityZopim.class));
            startActivity(new Intent(ActivityHome.this, ZopimChatActivity.class));
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_home, container, false);
           // TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            //textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Fragment f=new Fragment();
            if(position==0){
                f= new HomeFragment();
            }if(position==1){
                f= new MyAccountFragment();
            }
            return f;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }




    private void prepareAnimationObjects() {
        // TODO Auto-generated method stub
        if((android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) ){
            moveLeft= ObjectAnimator.ofFloat(decorRoot, "translationX", 0f, 0f);
            moveRight= ObjectAnimator.ofFloat(decorRoot,"translationX", 0f, 0f);
            moveDown = ObjectAnimator.ofFloat(decorRoot, "translationY", 0f, 70f);
            moveUp = ObjectAnimator.ofFloat(decorRoot, "translationY", 70f, 0f);
            scaleX= ObjectAnimator.ofFloat(decorRoot, "scaleX", 0.8f);
            scaleY= ObjectAnimator.ofFloat(decorRoot, "scaleY", 0.8f);
            scaleBackX= ObjectAnimator.ofFloat(decorRoot, "scaleX", 1.0f);
            scaleBackY= ObjectAnimator.ofFloat(decorRoot, "scaleY", 1.0f);
        }else{
            if(screen_width!=0){
                // move left to 80% of screen width , need to callibrate this for S5 , HTC one
                moveLeft= ObjectAnimator.ofFloat(decorRoot, "translationX", 0f, -(float)(screen_width*0.8));
                moveRight= ObjectAnimator.ofFloat(decorRoot,"translationX", -(float)(screen_width*0.8), 0f);
            }else{
                moveLeft= ObjectAnimator.ofFloat(decorRoot, "translationX", 0f, -500f);
                moveRight= ObjectAnimator.ofFloat(decorRoot,"translationX", -500f, 0f);
            }


            moveDown = ObjectAnimator.ofFloat(decorRoot, "translationY", 0f, 100f);
            moveUp = ObjectAnimator.ofFloat(decorRoot, "translationY", 100f, 0f);
            scaleX= ObjectAnimator.ofFloat(decorRoot, "scaleX", 0.8f);
            scaleY= ObjectAnimator.ofFloat(decorRoot, "scaleY", 0.8f);
            scaleBackX= ObjectAnimator.ofFloat(decorRoot, "scaleX", 1.0f);
            scaleBackY= ObjectAnimator.ofFloat(decorRoot, "scaleY", 1.0f);
        }


        // adding animation listners to set margins on pre honeycomb devices
        moveLeft.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator arg0) {
                // TODO Auto-generated method stub
                if((android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) ){

                    rootViewParams=(FrameLayout.LayoutParams)decorRoot.getLayoutParams();
                }

            }

            @Override
            public void onAnimationRepeat(Animator arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animator arg0) {
                // TODO Auto-generated method stub
                if((android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) ){
                    getSupportActionBar().hide();
                    rootViewParams.rightMargin = rootViewParams.rightMargin + 370;
                    rootViewParams.topMargin=10;
                    rootViewParams.bottomMargin=20;
                    rootViewParams.leftMargin=0;

                    decorRoot.setLayoutParams(rootViewParams);

                }


            }

            @Override
            public void onAnimationCancel(Animator arg0) {
                // TODO Auto-generated method stub

            }
        });


        moveRight.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator arg0) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onAnimationRepeat(Animator arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animator arg0) {
                if((android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.HONEYCOMB) ){
                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                            FrameLayout.LayoutParams.MATCH_PARENT,
                            FrameLayout.LayoutParams.MATCH_PARENT
                    );
                    //params.setMargins(LeftMargin, TopMargin, lParams.rightMargin, lParams.bottomMargin);
                    params.setMargins(0, 0,0, 0);
                    decorRoot.setLayoutParams(params);
                    decorRoot.requestLayout();


                }

            }

            @Override
            public void onAnimationCancel(Animator arg0) {
                // TODO Auto-generated method stub

            }
        });


        // configure pre honeycomb animation objects
        menu_disappear = AnimationUtils.loadAnimation(this, R.anim.menu_disappear);
        menu_disappear.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub

                //	AnimatorProxy.wrap(decorRoot).setTranslationX(-screen_width/2.0f);
                //AnimatorProxy.wrap(decorRoot).setScaleX(0.7f);
                //AnimatorProxy.wrap(decorRoot).setScaleY(0.7f);
				/*				FrameLayout.LayoutParams lParams = (FrameLayout.LayoutParams) decorRoot.getLayoutParams();
				int LeftMargin=  lParams.leftMargin;
				int TopMargin= lParams.topMargin;

				FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
				        FrameLayout.LayoutParams.MATCH_PARENT,
				        FrameLayout.LayoutParams.MATCH_PARENT
				);
				params.setMargins(LeftMargin, TopMargin, lParams.rightMargin, lParams.bottomMargin);
				decorRoot.setLayoutParams(params);
				 */


            }
        });


        menu_appear = AnimationUtils.loadAnimation(this, R.anim.menu_appear);
        menu_appear.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub

            }
        });


        zoom_exit=AnimationUtils.loadAnimation(this, R.anim.zoom_exit);
        zoom_exit.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub

            }
        });

        zoom_enter=AnimationUtils.loadAnimation(this, R.anim.zoom_enter);
        zoom_enter.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub

            }
        });




    }


    public void showMenuList(){

        animS= new AnimatorSet();

        overlayView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenuList();
            }
        });

        if(MENU_STATUS==0){
            MENU_STATUS=1;
            //listButtons.setEnabled(true);
            overlayView.setVisibility(View.VISIBLE);

            animS.play(moveLeft).with(scaleX).with(scaleY);
            animS.start();

        }else if(MENU_STATUS==1){
            overlayView.setVisibility(View.GONE);

            MENU_STATUS=0;
           // listButtons.setEnabled(false);


            animS.play(moveRight).with(scaleBackX).with(scaleBackY);
            animS.start();


        }
    }



    @Override
    public void onBackPressed(){
        if(MENU_STATUS==1){
            showMenuList();;
        }else{
            finish();
        }
    }


    @Override
    public void onResume(){
        super.onResume();
        String tickerCount = ActivityHome.this
                .getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                .getString("count_notif","0");
        mTxtVwTicker.setText(tickerCount);
    }


    private BroadcastReceiver performNearbySearchReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
           // update the notification counter here...
            //Toast.makeText(ActivityHome.this,"dsd",Toast.LENGTH_LONG).show();
            String tickerCount = ActivityHome.this
                                .getSharedPreferences("APP_PREFS",MODE_PRIVATE)
                                .getString("count_notif","0");
            mTxtVwTicker.setText(tickerCount);
        }
    };

}
